#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../login.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QApplication
# from PyQt5.QtWidgets import QDialog

datos = """\
Usuario: {0}
Contraseña: {1}"""

tabla = ()


class UiForm(QWidget):
    def __init__(self):
        super().__init__()
        self.contenido(self)
        self.setObjectName("form")
        self.resize(1024, 680)
        self.setMinimumSize(QtCore.QSize(1024, 680))
        self.setMaximumSize(QtCore.QSize(1024, 680))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../src/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        self.setStyleSheet("background-color: rgb(255, 255, 255);")

    @staticmethod
    def titulo():
        return 'Login'

    def contenido(self, form):
        self.lay_vertical = QtWidgets.QWidget(form)
        self.lay_vertical.setGeometry(QtCore.QRect(220, 140, 581, 291))
        self.lay_vertical.setObjectName("lay_vertical")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.lay_vertical)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lbl_nombre = QtWidgets.QLabel(self.lay_vertical)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_nombre.sizePolicy().hasHeightForWidth())
        self.lbl_nombre.setSizePolicy(sizePolicy)
        self.lbl_nombre.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_nombre.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_nombre.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_nombre.setObjectName("lbl_nombre")
        self.verticalLayout.addWidget(self.lbl_nombre)
        self.txt_nombre = QtWidgets.QLineEdit(self.lay_vertical)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_nombre.sizePolicy().hasHeightForWidth())
        self.txt_nombre.setSizePolicy(sizePolicy)
        self.txt_nombre.setMinimumSize(QtCore.QSize(579, 45))
        self.txt_nombre.setMaximumSize(QtCore.QSize(579, 45))
        self.txt_nombre.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_nombre.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_nombre.setObjectName("txt_nombre")
        self.verticalLayout.addWidget(self.txt_nombre)
        self.lbl_RFC = QtWidgets.QLabel(self.lay_vertical)
        self.lbl_RFC.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_RFC.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_RFC.setObjectName("lbl_RFC")
        self.verticalLayout.addWidget(self.lbl_RFC)
        self.txt_RFC = QtWidgets.QLineEdit(self.lay_vertical)
        self.txt_RFC.setMinimumSize(QtCore.QSize(579, 45))
        self.txt_RFC.setMaximumSize(QtCore.QSize(579, 45))
        self.txt_RFC.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_RFC.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_RFC.setObjectName("txt_RFC")
        self.verticalLayout.addWidget(self.txt_RFC)
        self.lbl_pass = QtWidgets.QLabel(self.lay_vertical)
        self.lbl_pass.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_pass.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_pass.setObjectName("lbl_pass")
        self.verticalLayout.addWidget(self.lbl_pass)
        self.txt_pass = QtWidgets.QLineEdit(self.lay_vertical)
        self.txt_pass.setEchoMode(QtWidgets.QLineEdit.Password)
        self.txt_pass.setMinimumSize(QtCore.QSize(579, 45))
        self.txt_pass.setMaximumSize(QtCore.QSize(579, 45))
        self.txt_pass.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_pass.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        # self.txt_pass.setOverwriteMode(True)
        self.txt_pass.setInputMethodHints(QtCore.Qt.ImhHiddenText)
        self.txt_pass.setObjectName("txt_pass")
        self.verticalLayout.addWidget(self.txt_pass)
        self.btn_login = QtWidgets.QPushButton(form)
        self.btn_login.setGeometry(QtCore.QRect(474, 530, 76, 76))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../src/login.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_login.setIcon(icon1)
        self.btn_login.setIconSize(QtCore.QSize(71, 71))
        self.btn_login.setFlat(True)
        self.btn_login.setObjectName("btn_login")
        # btn_login.clicked.connect(self.clic(self.txt_nombre.text(), self.txt_pass.text(), self.txt_RFC.text()))
        #     QtWidgets.QMessageBox.Warning(self, QtWidgets.QMessageBox(), 'Error', 'No has ingresado dato alguno')
        # btn_login.clicked.connect(validar())
        # self.txt_pass.textChanged(tabla[1].append(self.txt_pass.text()))
        # self.txt_nombre.textChanged(tabla[0].append(self.txt_nombre.text()))
        # self.txt_RFC.textChanged(tabla[2].append(self.txt_RFC.text()))

        self.retranslate_ui(form)
        QtCore.QMetaObject.connectSlotsByName(form)
        form.setTabOrder(self.txt_nombre, self.txt_RFC)
        form.setTabOrder(self.txt_RFC, self.txt_pass)
        form.setTabOrder(self.txt_pass, self.btn_login)
        
    def encabezado(self, form):
        self.fra_encabezado = QtWidgets.QFrame(form)
        self.fra_encabezado.setGeometry(QtCore.QRect(0, 0, 1024, 60))
        self.fra_encabezado.setMinimumSize(QtCore.QSize(949, 60))
        self.fra_encabezado.setMaximumSize(QtCore.QSize(1024, 60))
        self.fra_encabezado.setStyleSheet("QFrame{background-color: #14a085;}")
        self.fra_encabezado.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.fra_encabezado.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fra_encabezado.setObjectName("fra_encabezado")
        self.lbl_titulo = QtWidgets.QLabel(self.fra_encabezado)
        self.lbl_titulo.setGeometry(QtCore.QRect(222, 6, 580, 50))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_titulo.sizePolicy().hasHeightForWidth())
        self.lbl_titulo.setSizePolicy(sizePolicy)
        self.lbl_titulo.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_titulo.setObjectName("lbl_titulo")
        self.fra_ctrl_ventana = QtWidgets.QFrame(self.fra_encabezado)
        self.fra_ctrl_ventana.setGeometry(QtCore.QRect(949, 20, 55, 20))
        self.fra_ctrl_ventana.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.fra_ctrl_ventana.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fra_ctrl_ventana.setObjectName("frm_ctrl_ventana")
        self.btn_minimizar = QtWidgets.QPushButton(self.fra_ctrl_ventana)
        self.btn_minimizar.setGeometry(QtCore.QRect(0, 0, 20, 20))
        self.btn_minimizar.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../src/minimizar.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_minimizar.setIcon(icon1)
        self.btn_minimizar.setIconSize(QtCore.QSize(20, 20))
        self.btn_minimizar.setFlat(True)
        self.btn_minimizar.setObjectName("btn_minimizar")
        self.btn_salir = QtWidgets.QPushButton(self.fra_ctrl_ventana)
        self.btn_salir.setGeometry(QtCore.QRect(35, 0, 20, 20))
        self.btn_salir.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("../src/cerrar.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_salir.setIcon(icon2)
        self.btn_salir.setIconSize(QtCore.QSize(20, 20))
        self.btn_salir.setFlat(True)
        self.btn_salir.setObjectName("btn_salir")
        self.lbl_titulo.setText("Login")

    def retranslate_ui(self, form):
        _translate = QtCore.QCoreApplication.translate
        form.setWindowTitle(_translate("form", "Login"))
        self.lbl_nombre.setText(_translate("form", "Nombre completo"))
        self.lbl_RFC.setText(_translate("form", "RFC"))
        self.txt_pass.setPlaceholderText(_translate("Form", "●●●●●●"))
        self.lbl_pass.setText(_translate("form", "Contraseña"))
        self.btn_login.setShortcut(_translate("form", "Return"))


# def validar():
#     nombre = tabla[0]
#     contra = tabla[1]
#     rfc = tabla[2]
#     if nombre is not '' and contra is not '' and rfc is not '':
#         print('Datos\n', datos.format(nombre, contra, rfc))
#     else:
#         print("Te hace falta llenar todos los campos")

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    x = UiForm()
    x.show()
    sys.exit(app.exec_())
