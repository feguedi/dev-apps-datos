#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5 import QtGui, QtWidgets, QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QWidget, QDialog, QDesktopWidget, QFrame
from PyQt5.QtCore import QCoreApplication
import login_ui
import ayuda_ui
import carteraClientes.carteraClientes_ui as carteraClientes
# import comprobar_ui
import conceptos_ui
import datosFiscales_ui
import facturaConceptos_ui

__author__ = 'nger'
__version__ = '0.0.1'

control = {
    1: 'Login',
    2: 'Panel de Usuario',
    3: 'Facturación',
    4: 'Agregar Unidades',
    5: 'Cartera de Clientes'
}

titulo_str = 2
estilo = """QPushButton:pressed {
background-color: transparent;
border: none;
}
"""

"""
Este será el main donde serán cargadas las interfaces de las diferentes ventanas
creadas. Este documento manipulará los estados para las animaciones de los objetos
de las ventanas. Irá llamando de una en una dependiendo cuáles sean las acciones
del usuario.
Para programar los accesos a datos, se tendrá que hacer directamente en la
interfaz que tiene dicha responsabilidad (conceptos_ui.py / datosFiscales_ui.py /
facturaConceptos_ui.py).
Faltan de convertir carteraClientes_ui.py e images.qrc, aún no están terminadas.
"""


class Interfaz(QWidget):
    def __init__(self):
        super(Interfaz, self).__init__()
        self.formulario(self)
        if titulo_str != 1:
            self.menu(self)
        screen = QDesktopWidget().availableGeometry()
        y_pos = (screen.height() / 2) - 340
        x_pos = (screen.width() / 2) - 512
        self.move(x_pos, y_pos)
        # self.frm = QtCore.Qt.ApplicationModal(self)

    def formulario(self, frm):
        frm.setObjectName('Form')
        # self.frm.setWindowModality(QtCore.Qt.ApplicationModal)
        frm.resize(1024, 680)
        frm.setMinimumSize(QtCore.QSize(1024, 680))
        frm.setMaximumSize(QtCore.QSize(1024, 680))
        icono = QtGui.QIcon()
        icono.addPixmap(QtGui.QPixmap("../src/logo.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        frm.setWindowIcon(icono)
        frm.setStyleSheet("background-color: rgb(255, 255, 255);")
        frm.setWindowTitle('Sistema de Facturación')
        print('Se cargó el formulario')
        self.encabezado()
        # f = frm.windowFlags()
        # f |= Qt.FramelessWindowHint
        # frm.setWindowFlags(f)

    def encabezado(self):
        self.fra_encabezado = QtWidgets.QFrame(self)
        self.lbl_titulo = QtWidgets.QLabel(self.fra_encabezado)
        self.fra_ctrl_ventana = QtWidgets.QFrame(self.fra_encabezado)
        self.fra_encabezado.setMinimumSize(QtCore.QSize(949, 60))
        self.fra_encabezado.setMaximumSize(QtCore.QSize(1024, 60))
        self.fra_encabezado.setStyleSheet("QFrame{background-color: #14a085;}")
        self.fra_encabezado.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.fra_encabezado.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fra_encabezado.setObjectName("fra_encabezado")
        self.lbl_titulo.setGeometry(QtCore.QRect(222, 6, 580, 50))
        self.lbl_titulo.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_titulo.setStyleSheet('background: transparent;')
        fuente = QtGui.QFont()
        fuente.setFamily("Roboto")
        fuente.setPointSize(24)
        fuente.setWeight(QtGui.QFont.Normal)
        self.lbl_titulo.setFont(fuente)
        # self.lbl_titulo.setFont(QtGui.QFont.setStyle("font: 25 24pt 'Roboto';"))
        # self.lbl_titulo.setStyle("font: 25 24pt \"Roboto\";")
        self.lbl_titulo.setObjectName("lbl_titulo")
        self.fra_ctrl_ventana.setGeometry(QtCore.QRect(949, 20, 55, 20))
        self.fra_ctrl_ventana.setStyleSheet('background: transparent;'
                                            'QFrame::fra_ctrl_ventana:hover {'
                                            'QPushButton::btn_minimizar {image: src(../src/minimizar-1.svg);};'
                                            'QPushButton::btn_salir {image: src(../src/salir-1.svg);};'
                                            '}')
        self.fra_ctrl_ventana.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.fra_ctrl_ventana.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fra_ctrl_ventana.setObjectName("frm_ctrl_ventana")
        self.btn_minimizar = QtWidgets.QPushButton(self.fra_ctrl_ventana)
        self.btn_minimizar.setGeometry(QtCore.QRect(0, 0, 20, 20))
        self.btn_minimizar.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_minimizar.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../src/minimizar.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_minimizar.setIcon(icon1)
        self.btn_minimizar.setIconSize(QtCore.QSize(20, 20))
        self.btn_minimizar.setFlat(True)
        self.btn_minimizar.clicked.connect(minimizar)
        self.btn_minimizar.setStyleSheet('QPushButton:pressed {'
                                         'background-color: transparent;'
                                         'border: none;}'
                                         '@hover {'
                                         'image: src(../src/minimizar-1.svg);'
                                         '}')
        self.btn_minimizar.setObjectName("btn_minimizar")
        btn_salir = QtWidgets.QPushButton(self.fra_ctrl_ventana)
        btn_salir.setGeometry(QtCore.QRect(35, 0, 20, 20))
        btn_salir.setFocusPolicy(QtCore.Qt.NoFocus)
        btn_salir.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("../src/cerrar.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        btn_salir.setIcon(icon2)
        btn_salir.setIconSize(QtCore.QSize(20, 20))
        btn_salir.setFlat(True)
        btn_salir.clicked.connect(salir)
        btn_salir.setStyleSheet('QPushButton:pressed {'
                                'background-color: transparent;'
                                'border: none;}'
                                '@hover {'
                                'image: url(../src/cerrar-1.svg);'
                                '}')
        btn_salir.setObjectName("btn_salir")
        if titulo_str == 1:
            self.lbl_titulo.setText(control.get(1))
        elif titulo_str == 2:
            self.fra_encabezado.setGeometry(QtCore.QRect(76, 0, 948, 60))
            self.fra_ctrl_ventana.setGeometry(QtCore.QRect(873, 20, 55, 20))
            self.lbl_titulo.setGeometry(QtCore.QRect(183, 6, 581, 50))
            self.lbl_titulo.setText(control.get(2))
        elif titulo_str == 3:
            self.fra_encabezado.setGeometry(QtCore.QRect(76, 0, 948, 60))
            self.fra_ctrl_ventana.setGeometry(QtCore.QRect(873, 20, 55, 20))
            self.lbl_titulo.setGeometry(QtCore.QRect(183, 6, 581, 50))
            self.lbl_titulo.setText(control.get(3))
        elif titulo_str == 4:
            self.fra_encabezado.setGeometry(QtCore.QRect(76, 0, 948, 60))
            self.fra_ctrl_ventana.setGeometry(QtCore.QRect(873, 20, 55, 20))
            self.lbl_titulo.setGeometry(QtCore.QRect(183, 6, 581, 50))
            self.lbl_titulo.setText(control.get(4))
        elif titulo_str == 5:
            self.fra_encabezado.setGeometry(QtCore.QRect(76, 0, 948, 60))
            self.fra_ctrl_ventana.setGeometry(QtCore.QRect(543, 20, 55, 20))
            self.lbl_titulo.setGeometry(QtCore.QRect(183, 6, 581, 50))
            self.lbl_titulo.setText(control.get(5))
        print('Se cargó el encabezado')

    def mouseMoveEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.move(event.globalPos().x() - self.drag_position.x(),
                      event.globalPos().y() - self.drag_position.y())
        event.accept()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.drag_position = event.globalPos() - self.pos()
        event.accept()

    # def changeEvent(self, e):
    #     if e.type() == QtCore.QEvent.WindowStateChange and self.isMinimized():
    #         self.hide()
    #         e.accept()
    #         return
    #     else:
    #         super(Interfaz, self).changeEvent(e)

    def menu(self, formulario):
        fra_menu = QtWidgets.QFrame(formulario)
        # btn_datosfisc_emisor = QtWidgets.QPushButton(fra_menu)
        # btn_cartera_clientes = QtWidgets.QPushButton(fra_menu)
        # btn_conceptos = QtWidgets.QPushButton(fra_menu)
        # btn_unidades = QtWidgets.QPushButton(fra_menu)
        # btn_ayuda = QtWidgets.QPushButton(fra_menu)
        # btn_salir = QtWidgets.QPushButton(fra_menu)
        fra_menu.setGeometry(QtCore.QRect(0, 0, 76, 680))
        fra_menu.setStyleSheet("background-color: rgb(0, 50, 50);")
        fra_menu.setFrameShape(QtWidgets.QFrame.StyledPanel)
        fra_menu.setFrameShadow(QtWidgets.QFrame.Raised)
        fra_menu.setObjectName('Menu')
        self.btn_logout = QtWidgets.QPushButton(fra_menu)
        self.btn_logout.setGeometry(QtCore.QRect(13, 610, 50, 50))
        self.btn_logout.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_logout.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("../src/exit.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        icon3.addPixmap(QtGui.QPixmap("../src/exit.svg"), QtGui.QIcon.Selected, QtGui.QIcon.On)
        icon3.addPixmap(QtGui.QPixmap("../src/exit.svg"), QtGui.QIcon.Selected, QtGui.QIcon.Off)
        self.btn_logout.setIcon(icon3)
        self.btn_logout.setIconSize(QtCore.QSize(45, 45))
        self.btn_logout.setFlat(True)
        # self.btn_logout.clicked.connect(imprimir('Logout'))
        self.btn_logout.setStyleSheet('QPushButton:pressed {'
                                      'background-color: transparent;'
                                      'border: none;}'
                                      'QToolTip{'
                                      'background-color: black;'
                                      'color: white;'
                                      'position: absolute;'
                                      'top: 50%;'
                                      'right: 100%; /* To the left of the tooltip */'
                                      'margin-top: -5px;'
                                      'border-width: 5px;'
                                      'border-style: solid;'
                                      'border-color: transparent black transparent transparent;'''
                                      '}')
        self.btn_logout.setObjectName("btn_logout")
        self.btn_cartera_clientes = QtWidgets.QPushButton(fra_menu)
        self.btn_cartera_clientes.setGeometry(QtCore.QRect(13, 250, 50, 50))
        self.btn_cartera_clientes.setMinimumSize(QtCore.QSize(50, 50))
        self.btn_cartera_clientes.setMaximumSize(QtCore.QSize(50, 50))
        self.btn_cartera_clientes.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_cartera_clientes.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("../src/notebook.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        icon4.addPixmap(QtGui.QPixmap("../src/notebook.svg"), QtGui.QIcon.Selected, QtGui.QIcon.On)
        icon4.addPixmap(QtGui.QPixmap("../src/notebook.svg"), QtGui.QIcon.Selected, QtGui.QIcon.Off)
        self.btn_cartera_clientes.setIcon(icon4)
        self.btn_cartera_clientes.setIconSize(QtCore.QSize(45, 45))
        self.btn_cartera_clientes.setFlat(True)
        self.btn_cartera_clientes.setStyleSheet('QPushButton:pressed {'
                                                'background-color: transparent;'
                                                'border: none;}'
                                                'QToolTip{'
                                                'background-color: black;'
                                                'color: white;'
                                                'position: absolute;'
                                                'top: 50%;'
                                                'right: 100%; /* To the left of the tooltip */'
                                                'margin-top: -5px;'
                                                'border-width: 5px;'
                                                'border-style: solid;'
                                                'border-color: transparent black transparent transparent;'''
                                                '}')
        # self.btn_cartera_clientes.clicked.connect(imprimir("Cartera de Clientes"))
        self.btn_cartera_clientes.setObjectName("btn_cartera_clientes")
        self.btn_conceptos = QtWidgets.QPushButton(fra_menu)
        self.btn_conceptos.setGeometry(QtCore.QRect(13, 320, 50, 50))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_conceptos.sizePolicy().hasHeightForWidth())
        self.btn_conceptos.setSizePolicy(sizePolicy)
        self.btn_conceptos.setMinimumSize(QtCore.QSize(50, 50))
        self.btn_conceptos.setMaximumSize(QtCore.QSize(50, 50))
        self.btn_conceptos.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_conceptos.setText("")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap("../src/notepad.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        icon5.addPixmap(QtGui.QPixmap("../src/notepad.svg"), QtGui.QIcon.Selected, QtGui.QIcon.On)
        icon5.addPixmap(QtGui.QPixmap("../src/notepad.svg"), QtGui.QIcon.Selected, QtGui.QIcon.Off)
        self.btn_conceptos.setIcon(icon5)
        self.btn_conceptos.setIconSize(QtCore.QSize(45, 45))
        self.btn_conceptos.setFlat(True)
        self.btn_conceptos.setStyleSheet('QPushButton:pressed {'
                                         'background-color: transparent;'
                                         'border: none;}'
                                         'QToolTip{'
                                         'background-color: black;'
                                         'color: white;'
                                         'position: absolute;'
                                         'top: 50%;'
                                         'right: 100%; /* To the left of the tooltip */'
                                         'margin-top: -5px;'
                                         'border-width: 5px;'
                                         'border-style: solid;'
                                         'border-color: transparent black transparent transparent;'''
                                         '}')
        self.btn_conceptos.setObjectName("btn_conceptos")
        # self.btn_conceptos.clicked.connect(imprimir("Conceptos"))
        self.btn_unidades = QtWidgets.QPushButton(fra_menu)
        self.btn_unidades.setGeometry(QtCore.QRect(13, 390, 50, 50))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_unidades.sizePolicy().hasHeightForWidth())
        self.btn_unidades.setSizePolicy(sizePolicy)
        self.btn_unidades.setMinimumSize(QtCore.QSize(50, 50))
        self.btn_unidades.setMaximumSize(QtCore.QSize(50, 50))
        self.btn_unidades.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_unidades.setText("")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap("../src/list.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        icon6.addPixmap(QtGui.QPixmap("../src/list.svg"), QtGui.QIcon.Selected, QtGui.QIcon.On)
        icon6.addPixmap(QtGui.QPixmap("../src/list.svg"), QtGui.QIcon.Selected, QtGui.QIcon.Off)
        self.btn_unidades.setIcon(icon6)
        self.btn_unidades.setIconSize(QtCore.QSize(45, 45))
        self.btn_unidades.setFlat(True)
        self.btn_unidades.setStyleSheet('QPushButton:pressed {'
                                        'background-color: transparent;'
                                        'border: none;}'
                                        'QToolTip{'
                                        'background-color: black;'
                                        'color: white;'
                                        'position: absolute;'
                                        'top: 50%;'
                                        'right: 100%; /* To the left of the tooltip */'
                                        'margin-top: -5px;'
                                        'border-width: 5px;'
                                        'border-style: solid;'
                                        'border-color: transparent black transparent transparent;'''
                                        '}')
        # self.btn_unidades.clicked.connect(imprimir("Unidades"))
        self.btn_unidades.setObjectName("btn_unidades")
        self.btn_datosfisc_emisor = QtWidgets.QPushButton(fra_menu)
        self.btn_datosfisc_emisor.setGeometry(QtCore.QRect(13, 180, 50, 50))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_datosfisc_emisor.sizePolicy().hasHeightForWidth())
        self.btn_datosfisc_emisor.setSizePolicy(sizePolicy)
        self.btn_datosfisc_emisor.setMinimumSize(QtCore.QSize(50, 50))
        self.btn_datosfisc_emisor.setMaximumSize(QtCore.QSize(50, 50))
        self.btn_datosfisc_emisor.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_datosfisc_emisor.setText("")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap("../src/id-card-4.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_datosfisc_emisor.setIcon(icon7)
        self.btn_datosfisc_emisor.setIconSize(QtCore.QSize(45, 45))
        self.btn_datosfisc_emisor.setFlat(True)
        self.btn_datosfisc_emisor.setStyleSheet('QPushButton:pressed {'
                                                'background-color: transparent;'
                                                'border: none;}'
                                                'QToolTip{'
                                                'background-color: black;'
                                                'color: white;'
                                                'position: absolute;'
                                                'top: 50%;'
                                                'right: 100%; /* To the left of the tooltip */'
                                                'margin-top: -5px;'
                                                'border-width: 5px;'
                                                'border-style: solid;'
                                                'border-color: transparent black transparent transparent;'''
                                                '}')
        # self.btn_datosfisc_emisor.clicked.connect(imprimir('Datos Fiscales'))
        self.btn_datosfisc_emisor.setObjectName("btn_datosfisc_emisor")
        self.btn_ayuda = QtWidgets.QPushButton(fra_menu)
        self.btn_ayuda.setGeometry(QtCore.QRect(13, 460, 50, 50))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_ayuda.sizePolicy().hasHeightForWidth())
        self.btn_ayuda.setSizePolicy(sizePolicy)
        self.btn_ayuda.setMinimumSize(QtCore.QSize(50, 50))
        self.btn_ayuda.setMaximumSize(QtCore.QSize(50, 50))
        self.btn_ayuda.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_ayuda.setText("")
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap("../src/info.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        icon8.addPixmap(QtGui.QPixmap("../src/info.svg"), QtGui.QIcon.Selected, QtGui.QIcon.On)
        icon8.addPixmap(QtGui.QPixmap("../src/info.svg"), QtGui.QIcon.Selected, QtGui.QIcon.Off)
        self.btn_ayuda.setIcon(icon8)
        self.btn_ayuda.setIconSize(QtCore.QSize(45, 45))
        self.btn_ayuda.setFlat(True)
        self.btn_ayuda.setStyleSheet('QPushButton:pressed {'
                                     'background-color: transparent;'
                                     'border: none;}'
                                     'QToolTip{'
                                     'background-color: black;'
                                     'color: white;'
                                     'position: absolute;'
                                     'top: 50%;'
                                     'right: 100%; /* To the left of the tooltip */'
                                     'margin-top: -5px;'
                                     'border-width: 5px;'
                                     'border-style: solid;'
                                     'border-color: transparent black transparent transparent;'''
                                     '}')
        # self.btn_ayuda.clicked.connect(imprimir('Ayuda'))
        self.btn_ayuda.setObjectName("btn_ayuda")
        self.btn_ayuda.setToolTip('Ayuda')
        self.btn_datosfisc_emisor.setToolTip('Datos Fiscales')
        self.btn_unidades.setToolTip('Unidades')
        self.btn_conceptos.setToolTip('Concetos')
        self.btn_cartera_clientes.setToolTip('Cartera de Clientes')
        self.btn_logout.setToolTip('Cerrar Sesión')
        print('Se cargó el menú')


def imprimir(a):
    print("Clic en {0}".format(a))


def salir():
    exit()


def minimizar():
    print("Intentando minimizar")
    return Interfaz.showMinimized


def titulo(a):
    if a is 1:
        return control.get(1)
    elif a is 2:
        return control.get(2)
    elif a is 3:
        return control.get(3)
    elif a is 4:
        return control.get(4)
    elif a is 5:
        return control.get(5)


"""
Para definir una acción en un botón habrá que agregar al elemento la llamada a una
función que fuera a hacer dicha acción. Ejemplo:

self.boton.clicked.connect(self.funcion_ejemplo)

def funcion_ejemplo():
    print('Haz hecho click')

Esto para cuando la función a la que se está llamando no tiene parámetros y puede
estar adentro o fuera de la clase a la que pertenece el botón.
"""


class Login(QDialog, login_ui.UiForm):
    def __init__(self, parent=None):
        super(Login, self).__init__(parent)
        # self.setup_ui(self)
        self.contenido(self)


class Ayuda(QDialog, ayuda_ui.UiForm):
    def __init__(self):
        super().__init__()
        # self.setup_ui(self)
        self.contenido(self)


class CarteraClientes(QDialog, carteraClientes.UiForm):
    def __init__(self):
        super().__init__()
        # self.setup_ui(self)
        self.contenido(self)


class Conceptos(QDialog, conceptos_ui.UiForm):
    def __init__(self):
        super().__init__()
        # self.setup_ui(self)
        self.contenido(self)


class DatosFiscales(QDialog, datosFiscales_ui.UiForm):
    def __init__(self):
        super().__init__()
        # self.setup_ui(self)
        self.contenido(self)


class Factura(QDialog, facturaConceptos_ui.UiForm):
    def __init__(self):
        super().__init__()
        # self.setup_ui(self)
        self.contenido(self)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    x = Interfaz()
    # f = x.windowFlags()
    # f |= Qt.FramelessWindowHint
    x.setWindowFlags(Qt.FramelessWindowHint)
    x.show()
    sys.exit(app.exec_())
