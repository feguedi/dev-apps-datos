/*
Created		05/05/2016
Modified		31/05/2016
Project		Sistema de Facturacion Electronica
Model			Facturacion Electronica
Company		Duky Programation
Author		164237 184540 219451 220708
Version		1.0.0
Database		PostgreSQL 8.1 
*/


/* Create Tables */


Create table Receptor
(
	recRFC Varchar(13) NOT NULL,
	recNombre Varchar(60),
	domId Varchar(5) NOT NULL,
 primary key (recRFC)
) Without Oids;


Create table Domicilio
(
	domId Varchar(5) NOT NULL,
	domCalle Varchar(30),
	domNoExterior Varchar(5),
	domNoInterior Varchar(5),
	domColonia Varchar(30),
	domLocalidad Varchar(30),
	domReferencia Varchar(60),
	domMunicipio Char(30),
	domEstado Varchar(20),
	domPais Varchar(30) NOT NULL,
	domCP Integer,
 primary key (domId)
) Without Oids;


Create table Comprobante
(
	conCantidad Integer NOT NULL,
	traTipoImpuesto Varchar(20) NOT NULL,
	recRFC Varchar(13) NOT NULL,
	folio Varchar(3) NOT NULL,
	serie Varchar(3) NOT NULL,
	fecha Date NOT NULL,
	sello Varchar(60) NOT NULL,
	formaPago Varchar(20) NOT NULL,
	version Varchar(20) NOT NULL,
	noCertificado Varchar(20) NOT NULL,
	certificado Char(20) NOT NULL,
	subtotal Numeric(10,2) NOT NULL,
	descuento Numeric(10,2) NOT NULL,
	motivoDescuento Char(20) NOT NULL,
	tipoCambio Char(20) NOT NULL,
	moneda Char(20) NOT NULL,
	total Numeric(10,2) NOT NULL,
	tipoDeComprobante Char(20) NOT NULL,
	metodoDePago Char(20) NOT NULL,
    primary key (folio)
) Without Oids;


Create table Traslado
(
	traTipoImpuesto Varchar(20) NOT NULL,
	traTaza Numeric(5,2),
	traSubTotal Numeric(10,2),
    primary key (traTipoImpuesto)
) Without Oids;


Create table Login
(
	logId Serial NOT NULL,
	logUsuario Varchar(15),
	logPassword Varchar(15),
    primary key (logId)
) Without Oids;


Create table Producto
(
	proNoIdentificacion Varchar(10) NOT NULL,
	proNombre Varchar(20) NOT NULL,
	proPrecio Numeric(10,2) NOT NULL,
	proDescripcion Varchar(60) NOT NULL,
	proUnidad Varchar(20),
	folio Varchar(3) NOT NULL,
    primary key (proNoIdentificacion)
) Without Oids;



/* Create Foreign Keys */

Alter table Comprobante add  foreign key (recRFC) references Receptor (recRFC) on update cascade on delete restrict;

Alter table Receptor add  foreign key (domId) references Domicilio (domId) on update cascade on delete cascade;

Alter table Producto add  foreign key (folio) references Comprobante (folio) on update cascade on delete restrict;

Alter table Comprobante add  foreign key (traTipoImpuesto) references Traslado (traTipoImpuesto) on update cascade on delete restrict;
