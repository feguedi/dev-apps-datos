from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QDialog, QApplication, QWidget


class Form(QWidget):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.setup_ui(self)
        self.encabezado = QtWidgets.QFrame(self)
        self.encabezado.setStyleSheet("QFrame{background-color: #14a085;}")
        self.encabezado.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.encabezado.setFrameShadow(QtWidgets.QFrame.Raised)
        self.encabezado.setObjectName("frame")
        self.lbl_titulo = QtWidgets.QLabel(self.encabezado)
    
    def form(self):
        self.encabezado.setGeometry(QtCore.QRect(0, 0, 949, 60))
        self.lbl_titulo.setText('Encabezado de ejemplo')

    @staticmethod
    def setup_ui(ui):
        ui.setObjectName("Formulario")
        ui.setWindowModality(QtCore.Qt.ApplicationModal)
        ui.resize(1024, 684)
        ui.setMinimumSize(QtCore.QSize(1024, 684))
        ui.setMaximumSize(QtCore.QSize(1024, 684))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(''))

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    x = Form()
    x.show()
    sys.exit(app.exec_())
