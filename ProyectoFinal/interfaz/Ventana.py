# -*- coding: utf-8 -*-
import sys
if sys.platform is 'linux' or sys.platform is 'darwin':
    from _tkinter import *
    from _tkinter import TK
elif sys.platform is 'win32':
    from tkinter import *

colorFondo = "#FFFFFF"
colorHeader = "#14A085"
colorIzqBarra = "#003232"


def venPrincipal():
    vPrincipal = Tk()
    vPrincipal.title("Login")
    vPrincipal.geometry("1024x720+100+5")
    vPrincipal.resizable(width="FALSE", height="FALSE")
    vPrincipal.configure(background=colorFondo)
    vPrincipal.colormapwindows()
    header = Frame(vPrincipal, height=60, width=1366, bg=colorHeader)
    header.place(x=0, y=0)
    lbLogin = Label(vPrincipal, text="Login", bg=colorHeader, font="Arial 12").place(x=485, y=18)
    return vPrincipal


def venConcepto(vPrincipal):
    vConcepto = Toplevel(vPrincipal)
    vConcepto.title("Productos")
    vConcepto.geometry("1024x720+100+5")
    vConcepto.resizable(width="FALSE", height="FALSE")
    vConcepto.configure(background=colorFondo)
    vConcepto.colormapwindows()
    header = Frame(vConcepto, height=60, width=1366, bg=colorHeader)
    header.place(x=0, y=0)
    izqBarra = Frame(vConcepto, height=720, width=76, bg=colorIzqBarra)
    izqBarra.place(x=0, y=0)
    lbLogin = Label(vConcepto, text="Productos", bg=colorHeader, font="Arial 12").place(x=485, y=18)
    vConcepto.withdraw()
    return vConcepto


def venMain(vPrincipal):
    vMain = Toplevel(vPrincipal)
    vMain.title("Menú principal")
    vMain.geometry("1024x720+100+5")
    vMain.resizable(width="FALSE", height="FALSE")
    vMain.configure(background=colorFondo)
    vMain.colormapwindows()
    header = Frame(vMain, height=60, width=1366, bg=colorHeader)
    header.place(x=0, y=0)
    izqBarra = Frame(vMain, height=720, width=76, bg=colorIzqBarra)
    izqBarra.place(x=0, y=0)
    lbLogin = Label(vMain, text="Menú principal", bg=colorHeader, font="Arial 12").place(x=485, y=18)
    vMain.withdraw()
    return vMain


def venClientes(vPrincipal):
    vClientes = Toplevel(vPrincipal)
    vClientes.title("Clientes")
    vClientes.geometry("1024x720+100+5")
    vClientes.resizable(width="FALSE", height="FALSE")
    vClientes.configure(background=colorFondo)
    vClientes.colormapwindows()
    header = Frame(vClientes, height=60, width=1366, bg=colorHeader)
    header.place(x=0, y=0)
    izqBarra = Frame(vClientes, height=720, width=76, bg=colorIzqBarra)
    izqBarra.place(x=0, y=0)
    lbLogin = Label(vClientes, text="Clientes", bg=colorHeader, font="Arial 12").place(x=485, y=18)
    vClientes.withdraw()
    return vClientes


def venDatosEmi(vPrincipal):
    vDatosEmi = Toplevel(vPrincipal)
    vDatosEmi.title("Datos del Emisor")
    vDatosEmi.geometry("1024x720+100+5")
    vDatosEmi.resizable(width="FALSE", height="FALSE")
    vDatosEmi.configure(background=colorFondo)
    vDatosEmi.colormapwindows()
    header = Frame(vDatosEmi, height=60, width=1366, bg=colorHeader)
    header.place(x=0, y=0)
    izqBarra = Frame(vDatosEmi, height=720, width=76, bg=colorIzqBarra)
    izqBarra.place(x=0, y=0)
    lbLogin = Label(vDatosEmi, text="Emisor", bg=colorHeader, font="Arial 12").place(x=485, y=18)
    vDatosEmi.withdraw()
    return vDatosEmi


def venConfig(vPrincipal):
    vConfig = Toplevel(vPrincipal)
    vConfig.title("Registro")
    vConfig.geometry("1024x720+100+5")
    vConfig.resizable(width="FALSE", height="FALSE")
    vConfig.configure(background=colorFondo)
    vConfig.colormapwindows()
    header = Frame(vConfig, height=60, width=1366, bg=colorHeader)
    header.place(x=0, y=0)
    lbLogin = Label(vConfig, text="Configuración", bg=colorHeader, font="Arial 12").place(x=485, y=18)
    vConfig.withdraw()
    return vConfig


def venFactura(vPrincipal):
    vFac = Toplevel(vPrincipal)
    vFac.title("Crear Factura")
    vFac.geometry("1024x720+100+5")
    vFac.resizable(width="FALSE", height="FALSE")
    vFac.configure(background=colorFondo)
    vFac.colormapwindows()
    header = Frame(vFac, height=60, width=1366, bg=colorHeader)
    header.place(x=0, y=0)
    izqBarra = Frame(vFac, height=720, width=76, bg=colorIzqBarra)
    izqBarra.place(x=0, y=0)
    lbLogin = Label(vFac, text="Crear Factura", bg=colorHeader, font="Arial 12").place(x=485, y=18)
    vFac.withdraw()
    return vFac
