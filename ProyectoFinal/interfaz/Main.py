# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import ttk
import Ventana
import fun

venLogin = Ventana.venPrincipal()
venMain = Ventana.venMain(venLogin)
venClien = Ventana.venClientes(venLogin)
venConcep = Ventana.venConcepto(venLogin)
venDatosEmi = Ventana.venDatosEmi(venLogin)
venConfig = Ventana.venConfig(venLogin)
venFactura = Ventana.venFactura(venLogin)

imgLogin = PhotoImage(file="src/login.png")
imgIdCard = PhotoImage(file="src/btn_id-card-4.png")
imgNotepad = PhotoImage(file="src/btn_notepad.png")
imgNotebook = PhotoImage(file="src/btn_notebook.png")
imgList = PhotoImage(file="src/btn_list.png")
imgInfo = PhotoImage(file="src/btn_info.png")
imgPlus = PhotoImage(file="src/btn_plus.png")
imgError = PhotoImage(file="src/btn_error.png")
imgExit = PhotoImage(file="src/btn_exit.png")
imgEdit = PhotoImage(file="src/btn_edit.png")
imgIdCardM = PhotoImage(file="src/main_id-card-4.png")
imgNotepadM = PhotoImage(file="src/main_notepad.png")
imgNotebookM = PhotoImage(file="src/main_notebook.png")
imgListM = PhotoImage(file="src/main_list.png")
imgSuccess = PhotoImage(file="src/btn_success.png")

### Declaracion de Variables ###########################################################################################
varColorFondo = "#FFFFFF"
varColorIzqBarra = "#003232"
varFontTxtE = "MS 9"
varCantidad = list(range(1, 51))
flag = IntVar()
flag1 = IntVar()
flag.set(0)
flag1.set(1)
# Variables Login #------------------------
varUsu = StringVar()
varPass = StringVar()
# Variables Config #-----------------------
varServerCo = StringVar()
varUserCo = StringVar()
varPasswordCo = StringVar()
varDBCo = StringVar()
varUserLogCo = StringVar()
varPassLogCo = StringVar()
varCorreoCo = StringVar()
varNombreCo = StringVar()
varRFCCo = StringVar()
varCalleCo = StringVar()
varNoExCo = StringVar()
varColoniaCo = StringVar()
varLocalidadCo = StringVar()
varReferenciaCo = StringVar()
varMunicipioCo = StringVar()
varEstadoCo = StringVar()
varPaisCo = StringVar()
varCPCo = StringVar()
varRegimenCo = StringVar()
# Variables Datos Emisor #------------------
varlbNomEvar = StringVar()
varlbRFCEvar = StringVar()
varlbCorreoEvar = StringVar()
varlbCalleEvar = StringVar()
varlbNoExEvar = StringVar()
varlbColoniaEvar = StringVar()
varlbLocalidadEvar = StringVar()
varlbReferenciaEvar = StringVar()
varlbMunicipioEvar = StringVar()
varlbestadoEvar = StringVar()
varlbPaisEvar = StringVar()
varlbCPEvar = StringVar()
varlbRegimenEvar = StringVar()
listVarDatEmi = [varlbNomEvar, varlbRFCEvar, varlbCorreoEvar, varlbCalleEvar, varlbNoExEvar, varlbColoniaEvar,
                 varlbLocalidadEvar, varlbReferenciaEvar, varlbMunicipioEvar, varlbestadoEvar, varlbPaisEvar,
                 varlbCPEvar, varlbRegimenEvar]

### Canvas #############################################################################################################
cabLinSepPro = Frame(venConcep, height=20, width=900, bg="#464646").place(x=100, y=200)
canLinSepCli = Frame(venClien, height=20, width=900, bg="#464646").place(x=100, y=240)
canLinSepCon1 = Frame(venConfig, height=20, width=300, bg="#464646").place(x=0, y=90)
canLinSepCon2 = Frame(venConfig, height=20, width=300, bg="#464646").place(x=0, y=220)
canLinSepCon3 = Frame(venConfig, height=20, width=300, bg="#464646").place(x=0, y=300)
canLinSepFac = Frame(venFactura, height=20, width=900, bg="#464646").place(x=100, y=200)

### Labels #############################################################################################################
# Ventana Login #----------------------------
lbUsu = Label(venLogin, text="Usuario:", bg=varColorFondo, font="Serif 12").place(x=220, y=220)
lbPass = Label(venLogin, text="Contraseña:", bg=varColorFondo, font="Serif 12").place(x=220, y=320)

# Ventana Main #----------------------------
lbBien = Label(venMain, text="Bienvenido", bg=varColorFondo, font="MS 20").place(x=470, y=80)
lbDaEm = Label(venMain, text="Datos del Emisor", bg=varColorFondo, font="MS 22").place(x=210, y=240)
lbNuFa = Label(venMain, text="Nueva Factura", bg=varColorFondo, font="MS 22").place(x=670, y=240)
lbAgCl = Label(venMain, text="Agregar Clientes", bg=varColorFondo, font="MS 22").place(x=210, y=420)
lbAgPr = Label(venMain, text="Agregar Productos", bg=varColorFondo, font="MS 22").place(x=650, y=420)

# Ventana Factura #-------------------------
lbCliF = Label(venFactura, text="RFC del Cliente:", bg=varColorFondo, font=varFontTxtE).place(x=230, y=110)
lbProF = Label(venFactura, text="Producto:", bg=varColorFondo, font=varFontTxtE).place(x=230, y=150)
lbCanF = Label(venFactura, text="Cantidad:", bg=varColorFondo, font=varFontTxtE).place(x=500, y=150)
lbIdeFacLis = Label(venFactura, text="ID", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=110, y=200)
lbCanFacLis = Label(venFactura, text="Cantidad", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=180, y=200)
lbDesFacLis = Label(venFactura, text="Descripcion", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=390,
                                                                                                            y=200)
lbPreUnFacLis = Label(venFactura, text="Valor Unitario", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=750,
                                                                                                                 y=200)
lbPreImFacLis = Label(venFactura, text="Importe", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=900, y=200)

# Ventana Datos Emisor #------------------------
lbNomE = Label(venDatosEmi, text="Nombre:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=120)
lbNomEvar = Label(venDatosEmi, textvariable=varlbNomEvar, bg=varColorFondo, font=varFontTxtE).place(x=205, y=120)
lbRFCE = Label(venDatosEmi, text="RFC:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=160)
lbRFCEvar = Label(venDatosEmi, textvariable=varlbRFCEvar, bg=varColorFondo, font=varFontTxtE).place(x=187, y=160)
lbCorreoE = Label(venDatosEmi, text="Correo Eectrónico:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=200)
lbCorreoEvar = Label(venDatosEmi, textvariable=varlbCorreoEvar, bg=varColorFondo, font=varFontTxtE).place(x=255, y=200)
lbDomFisE = Label(venDatosEmi, text="Domicilio Fiscal", bg=varColorFondo, font="MS 12").place(x=170, y=250)
lbCalleE = Label(venDatosEmi, text="Calle:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=300)
lbCalleEvar = Label(venDatosEmi, textvariable=varlbCalleEvar, bg=varColorFondo, font=varFontTxtE).place(x=190, y=300)
lbNoExE = Label(venDatosEmi, text="Número Exterior:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=340)
lbNoExEvar = Label(venDatosEmi, textvariable=varlbNoExEvar, bg=varColorFondo, font=varFontTxtE).place(x=245, y=340)
lbColoniaE = Label(venDatosEmi, text="Colonia:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=380)
lbColoniaEvar = Label(venDatosEmi, textvariable=varlbColoniaEvar, bg=varColorFondo, font=varFontTxtE).place(x=200,
                                                                                                            y=380)
lbLocalidadE = Label(venDatosEmi, text="Localidad:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=420)
lbLocalidadEvar = Label(venDatosEmi, textvariable=varlbLocalidadEvar, bg=varColorFondo, font=varFontTxtE).place(x=215,
                                                                                                                y=420)
lbReferenciaE = Label(venDatosEmi, text="Referencia:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=460)
lbReferenciaEvar = Label(venDatosEmi, textvariable=varlbReferenciaEvar, bg=varColorFondo, font=varFontTxtE).place(x=223,
                                                                                                                  y=460)
lbMunicipioE = Label(venDatosEmi, text="Municipio:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=500)
lbMunicipioEvar = Label(venDatosEmi, textvariable=varlbMunicipioEvar, bg=varColorFondo, font=varFontTxtE).place(x=210,
                                                                                                                y=500)
lbEstadoE = Label(venDatosEmi, text="Estado:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=540)
lbestadoEvar = Label(venDatosEmi, textvariable=varlbestadoEvar, bg=varColorFondo, font=varFontTxtE).place(x=200, y=540)
lbPaisE = Label(venDatosEmi, text="Pais:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=580)
lbPaisEvar = Label(venDatosEmi, textvariable=varlbPaisEvar, bg=varColorFondo, font=varFontTxtE).place(x=187, y=580)
lbCPE = Label(venDatosEmi, text="Codigo Postal:", bg=varColorFondo, font=varFontTxtE).place(x=150, y=620)
lbCPEvar = Label(venDatosEmi, textvariable=varlbCPEvar, bg=varColorFondo, font=varFontTxtE).place(x=235, y=620)
# lbFolioE = Label(venDatosEmi, text = "Folio:", bg = varColorFondo, font = varFontTxtE).place(x = 700, y = 120)
# lbFolioEvar = Label(venDatosEmi, text = listDatosEmi[12], bg = varColorFondo, font = varFontTxtE).place(x = 740, y = 120)
lbRegimenE = Label(venDatosEmi, text="Regimen:", bg=varColorFondo, font=varFontTxtE).place(x=700, y=120)
lbRegimenEvar = Label(venDatosEmi, textvariable=varlbRegimenEvar, bg=varColorFondo, font=varFontTxtE).place(x=760,
                                                                                                            y=120)

# Ventana Clientes #------------------------
lbCliNom = Label(venClien, text="Nombre:", bg=varColorFondo, font="MS 8").place(x=100, y=100)
lbCliRFC = Label(venClien, text="RFC:", bg=varColorFondo, font="MS 8").place(x=100, y=130)
# lbCliRSo = Label(venClien, text = "Razon Social:", bg = varColorFondo, font = "MS 8").place(x =100 , y = 160)
lbCliCal = Label(venClien, text="Calle:", bg=varColorFondo, font="MS 8").place(x=405, y=100)
lbCliNIn = Label(venClien, text="No Interior:", bg=varColorFondo, font="MS 8").place(x=380, y=130)
lbCliNEx = Label(venClien, text="No Exterior:", bg=varColorFondo, font="MS 8").place(x=376, y=160)
lbCliCol = Label(venClien, text="Colonia:", bg=varColorFondo, font="MS 8").place(x=395, y=190)
lbCliLoc = Label(venClien, text="Localidad:", bg=varColorFondo, font="MS 8").place(x=570, y=100)
lbCliRef = Label(venClien, text="Referencia:", bg=varColorFondo, font="MS 8").place(x=564, y=130)
lbCliEst = Label(venClien, text="Estado:", bg=varColorFondo, font="MS 8").place(x=583, y=160)
lbCliMun = Label(venClien, text="Municipio:", bg=varColorFondo, font="MS 8").place(x=573, y=190)
lbCliPai = Label(venClien, text="País:", bg=varColorFondo, font="MS 8").place(x=810, y=100)
lbCliCP = Label(venClien, text="Codigo Postal:", bg=varColorFondo, font="MS 8").place(x=790, y=160)
lbRFCCliLis = Label(venClien, text="RFC", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=120, y=240)
lbNomCliLis = Label(venClien, text="Nombre", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=350, y=240)
lbRazCliLis = Label(venClien, text="Direccion", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=650, y=240)

# Ventana Concepto #------------------------
lbIdePro = Label(venConcep, text="ID:", bg=varColorFondo, font="MS 8").place(x=140, y=120)
lbNomPro = Label(venConcep, text="Nombre Producto:", bg=varColorFondo, font="MS 8").place(x=250, y=120)
lbDesPro = Label(venConcep, text="Descripcion:", bg=varColorFondo, font="MS 8").place(x=140, y=150)
lbUniPro = Label(venConcep, text="Unidad:", bg=varColorFondo, font="MS 8").place(x=485, y=120)
lbPrePro = Label(venConcep, text="Precio:", bg=varColorFondo, font="MS 8").place(x=670, y=120)
lbIdeProLis = Label(venConcep, text="ID", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=110, y=200)
lbNomProLis = Label(venConcep, text="Nombre Producto", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=170,
                                                                                                               y=200)
lbDesProLis = Label(venConcep, text="Descripcion", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=500, y=200)
lbPreProLis = Label(venConcep, text="Precio", fg=varColorFondo, bg="#464646", font="Courier 8").place(x=930, y=200)

# Ventana Cofiguracion #---------------------
lbCoDBCon = Label(venConfig, text="Configuracion de la Base de Datos", bg="#464646", fg=varColorFondo,
                  font="MS 8").place(x=30, y=90)
lbSerCon = Label(venConfig, text="Host:", bg=varColorFondo, font="MS 8").place(x=100, y=120)
lbUseCon = Label(venConfig, text="Usuario:", bg=varColorFondo, font="MS 8").place(x=510, y=120)
lbPasCon = Label(venConfig, text="Contraseña:", bg=varColorFondo, font="MS 8").place(x=100, y=170)
lbDaBCon = Label(venConfig, text="Nombre de la Base de Datos:", bg=varColorFondo, font="MS 8").place(x=510, y=170)
lbCoLoCon = Label(venConfig, text="Configuracion del Primer Login", bg="#464646", fg=varColorFondo, font="MS 8").place(
    x=30, y=220)
lbUseLoCon = Label(venConfig, text="Usuario:", bg=varColorFondo, font="MS 8").place(x=100, y=250)
lbPasLoCon = Label(venConfig, text="Contraseña:", bg=varColorFondo, font="MS 8").place(x=510, y=250)
lbEmCon = Label(venConfig, text="Configuracion de los Datos del Emisor", bg="#464646", fg=varColorFondo,
                font="MS 8").place(x=30, y=300)
lbCorCon = Label(venConfig, text="Correo:", bg=varColorFondo, font="MS 8").place(x=100, y=330)
lbNomCon = Label(venConfig, text="Nombre:", bg=varColorFondo, font="MS 8").place(x=510, y=330)
lbRFCCon = Label(venConfig, text="RFC:", bg=varColorFondo, font="MS 8").place(x=100, y=380)
lbCalCon = Label(venConfig, text="Calle:", bg=varColorFondo, font="MS 8").place(x=510, y=380)
lbNoExCon = Label(venConfig, text="Número Exterior:", bg=varColorFondo, font="MS 8").place(x=100, y=430)
lbColCon = Label(venConfig, text="Colonia:", bg=varColorFondo, font="MS 8").place(x=510, y=430)
lbLocCon = Label(venConfig, text="Localidad:", bg=varColorFondo, font="MS 8").place(x=100, y=480)
lbRefCon = Label(venConfig, text="Referencia:", bg=varColorFondo, font="MS 8").place(x=510, y=480)
lbMunCon = Label(venConfig, text="Municipio:", bg=varColorFondo, font="MS 8").place(x=100, y=530)
lbEstCon = Label(venConfig, text="Estado:", bg=varColorFondo, font="MS 8").place(x=510, y=530)
lbPaiCon = Label(venConfig, text="País:", bg=varColorFondo, font="MS 8").place(x=100, y=580)
lbCPCon = Label(venConfig, text="Codigo Postal:", bg=varColorFondo, font="MS 8").place(x=510, y=580)
# lbFolCon = Label(venConfig, text = "Folio:", bg = varColorFondo, font = "MS 8").place(x = 100, y= 630)
lbRegCon = Label(venConfig, text="Regimen:", bg=varColorFondo, font="MS 8").place(x=100, y=630)

### Entrys #############################################################################################################
# Ventana Login #----------------------------
enUsu = Entry(venLogin, width=45, font="Robot 18", bd=2, relief=SUNKEN, textvariable=varUsu).place(x=220, y=245)
enPass = Entry(venLogin, width=45, font="Robot 18", bd=2, relief=SUNKEN, show="*", textvariable=varPass).place(x=220,
                                                                                                               y=345)

# Ventana Clientes #------------------------
enCliNom = Entry(venClien, width=25, font="MS 8", bd=2, relief=SUNKEN).place(x=180, y=100)
enCliRFC = Entry(venClien, width=25, font="MS 8", bd=2, relief=SUNKEN).place(x=180, y=130)
# enCliRSo = Entry(venClien, width = 25, font = "MS 8", bd = 2, relief = SUNKEN).place(x =180 , y = 160)
enCliCal = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=440, y=100)
enCliNIn = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=440, y=130)
enCliNEx = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=440, y=160)
enCliCol = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=440, y=190)
enCliLoc = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=630, y=100)
enCliRef = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=630, y=130)
enCliEst = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=630, y=160)
enCliMun = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=630, y=190)
enCliPai = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=780, y=130)
enCliCP = Entry(venClien, width=15, font="MS 8", bd=2, relief=SUNKEN).place(x=780, y=190)
listEnClien = [enCliNom, enCliRFC, enCliCal, enCliNIn, enCliNEx, enCliCol, enCliLoc, enCliRef, enCliEst, enCliMun,
               enCliPai, enCliCP]

# Ventana Concepto #------------------------
enIdePro = Entry(venConcep, width=10, font="MS 8", bd=2, relief=SUNKEN)
enIdePro.place(x=157, y=120)
enNomPro = Entry(venConcep, width=20, font="Ms 8", bd=2, relief=SUNKEN)
enNomPro.place(x=345, y=120)
enDesPro = Entry(venConcep, width=60, font="Ms 8", bd=2, relief=SUNKEN)
enDesPro.place(x=205, y=150)
enUniPro = Entry(venConcep, width=20, font="Ms 8", bd=2, relief=SUNKEN)
enUniPro.place(x=527, y=120)
enPrePro = Entry(venConcep, width=12, font="Ms 8", bd=2, relief=SUNKEN)
enPrePro.place(x=710, y=120)
listEnProd = [enIdePro, enNomPro, enDesPro, enUniPro, enPrePro]

# Ventana Cofiguracion #---------------------
enSerCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varServerCo).place(x=130, y=120)
enUseCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varUserCo).place(x=555, y=120)
enPasCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varPasswordCo).place(x=167, y=170)
enDaBCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varDBCo).place(x=657, y=170)
enUseLoCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varUserLogCo).place(x=147, y=250)
enPasLoCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varPassLogCo).place(x=580, y=250)
enCorCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varCorreoCo).place(x=147, y=330)
enNomCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varNombreCo).place(x=560, y=330)
enRFCCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varRFCCo).place(x=135, y=380)
enCalCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varCalleCo).place(x=545, y=380)
enNoExCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varNoExCo).place(x=190, y=430)
enColCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varColoniaCo).place(x=560, y=430)
enLocCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varLocalidadCo).place(x=160, y=480)
enRefCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varReferenciaCo).place(x=580,
                                                                                                            y=480)
enMunCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varMunicipioCo).place(x=155, y=530)
enEstCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varEstadoCo).place(x=560, y=530)
enPaiCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varPaisCo).place(x=130, y=580)
enCPCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varCPCo).place(x=590, y=580)
# enFolCon = Entry(venConfig, width = 30, font = "MS 8", bd = 2, relief = SUNKEN, textvariable = varF).place(x = 130, y= 630)
enRegCon = Entry(venConfig, width=30, font="MS 8", bd=2, relief=SUNKEN, textvariable=varRegimenCo).place(x=150, y=630)

### Botones ############################################################################################################
# Ventana Login #----------------------------
btnLogin = Button(venLogin, image=imgLogin, relief=FLAT, bg=varColorFondo, cursor="hand2", height=75, width=66,
                  command=lambda: fun.ValidarLogin(venLogin, venMain, varUsu, varPass, flag)).place(x=474, y=540)

# Ventana Factura #--------------------------
btnIdCardF = Button(venFactura, image=imgIdCard, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                    cursor="hand2", command=lambda: fun.cV(venFactura, venDatosEmi, flag)).place(x=10, y=220)
btnNoteBookF = Button(venFactura, image=imgNotebook, relief=FLAT, bg=varColorIzqBarra,
                      activebackground=varColorIzqBarra, cursor="hand2",
                      command=lambda: fun.cV(venFactura, venClien, flag)).place(x=10, y=280)
btnNotePadF = Button(venFactura, image=imgNotepad, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                     cursor="hand2", command=lambda: fun.cV(venFactura, venConcep, flag)).place(x=10, y=350)
btnListF = Button(venFactura, image=imgList, relief=FLAT, bg=varColorIzqBarra, state=DISABLED,
                  activebackground=varColorIzqBarra, cursor="hand2").place(x=15, y=430)
btnExitF = Button(venFactura, image=imgExit, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                  cursor="hand2", command=lambda: fun.cV(venFactura, venMain, flag)).place(x=10, y=650)
btnPlusF = Button(venFactura, image=imgPlus, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                  cursor="hand2").place(x=730, y=140)
btnDelF = Button(venFactura, image=imgError, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                 cursor="hand2").place(x=780, y=140)
btnAceptarF = Button(venFactura, image=imgSuccess, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                     cursor="hand2").place(x=487, y=640)

# Ventana Main #----------------------------
btnIdCardM = Button(venMain, image=imgIdCard, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                    cursor="hand2", command=lambda: fun.cV(venMain, venDatosEmi, flag)).place(x=10, y=220)
btnNoteBookM = Button(venMain, image=imgNotebook, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                      cursor="hand2", command=lambda: fun.cV(venMain, venClien, flag)).place(x=10, y=280)
btnNotePadM = Button(venMain, image=imgNotepad, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                     cursor="hand2", command=lambda: fun.cV(venMain, venConcep, flag)).place(x=10, y=350)
btnListM = Button(venMain, image=imgList, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                  cursor="hand2", command=lambda: fun.cV(venMain, venFactura, flag)).place(x=15, y=430)
btnExitM = Button(venMain, image=imgExit, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                  cursor="hand2", command=lambda: fun.cV(venMain, venLogin, flag)).place(x=10, y=650)
btnDaEmM = Button(venMain, image=imgIdCardM, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                  cursor="hand2", command=lambda: fun.cV(venMain, venDatosEmi, flag)).place(x=290, y=300)
btnNuFakM = Button(venMain, image=imgListM, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                   cursor="hand2", command=lambda: fun.cV(venMain, venFactura, flag)).place(x=730, y=300)
btnAgClM = Button(venMain, image=imgNotebookM, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                  cursor="hand2", command=lambda: fun.cV(venMain, venClien, flag)).place(x=280, y=470)
btnAgPrM = Button(venMain, image=imgNotepadM, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                  cursor="hand2", command=lambda: fun.cV(venMain, venConcep, flag)).place(x=730, y=470)

# Ventana Datos Emisor #------------------------
btnIdCardE = Button(venDatosEmi, image=imgIdCard, relief=FLAT, bg=varColorIzqBarra, state=DISABLED,
                    activebackground=varColorIzqBarra, cursor="hand2").place(x=10, y=220)
btnNoteBookE = Button(venDatosEmi, image=imgNotebook, relief=FLAT, bg=varColorIzqBarra,
                      activebackground=varColorIzqBarra, cursor="hand2",
                      command=lambda: fun.cV(venDatosEmi, venClien, flag)).place(x=10, y=280)
btnNotePadE = Button(venDatosEmi, image=imgNotepad, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                     cursor="hand2", command=lambda: fun.cV(venDatosEmi, venConcep, flag)).place(x=10, y=350)
btnListE = Button(venDatosEmi, image=imgList, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                  cursor="hand2", command=lambda: fun.cV(venDatosEmi, venFactura, flag)).place(x=15, y=430)
btnExitE = Button(venDatosEmi, image=imgExit, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                  cursor="hand2", command=lambda: fun.cV(venDatosEmi, venMain, flag)).place(x=10, y=650)
btnModiE = Button(venDatosEmi, image=imgEdit, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                  cursor="hand2").place(x=930, y=660)

# Ventana Clientes #----------------------------
btnIdCardC = Button(venClien, image=imgIdCard, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                    cursor="hand2", command=lambda: fun.cV(venClien, venDatosEmi, flag)).place(x=10, y=220)
btnNoteBookC = Button(venClien, image=imgNotebook, relief=FLAT, bg=varColorIzqBarra, state=DISABLED,
                      activebackground=varColorIzqBarra, cursor="hand2").place(x=10, y=280)
btnNotePadC = Button(venClien, image=imgNotepad, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                     cursor="hand2", command=lambda: fun.cV(venClien, venConcep, flag)).place(x=10, y=350)
btnListC = Button(venClien, image=imgList, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                  cursor="hand2", command=lambda: fun.cV(venClien, venFactura, flag)).place(x=15, y=430)
btnPlusC = Button(venClien, image=imgPlus, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                  cursor="hand2").place(x=930, y=70)
btnDelC = Button(venClien, image=imgError, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                 cursor="hand2").place(x=930, y=190)
btnExitC = Button(venClien, image=imgExit, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                  cursor="hand2", command=lambda: fun.cV(venClien, venMain, flag)).place(x=10, y=650)
btnModiC = Button(venClien, image=imgEdit, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                  cursor="hand2").place(x=930, y=130)

# Ventana Concepto #------------------------
btnIdCardP = Button(venConcep, image=imgIdCard, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                    cursor="hand2", command=lambda: fun.cV(venConcep, venDatosEmi, flag)).place(x=10, y=220)
btnNoteBookP = Button(venConcep, image=imgNotebook, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                      cursor="hand2", command=lambda: fun.cV(venConcep, venClien, flag)).place(x=10, y=280)
btnNotePadP = Button(venConcep, image=imgNotepad, relief=FLAT, bg=varColorIzqBarra, state=DISABLED,
                     activebackground=varColorIzqBarra, cursor="hand2").place(x=10, y=350)
btnListP = Button(venConcep, image=imgList, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                  cursor="hand2", command=lambda: fun.cV(venConcep, venFactura, flag)).place(x=15, y=430)
btnPlusP = Button(venConcep, image=imgPlus, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                  cursor="hand2", command=lambda: fun.NuevoPro(lisPro, listEnProd, flag, btnModiP)).place(x=830, y=120)
btnDelP = Button(venConcep, image=imgError, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                 cursor="hand2", command=lambda: fun.EliminarPro(lisPro, listEnProd, flag, btnModiP)).place(x=950,
                                                                                                            y=120)
btnExitP = Button(venConcep, image=imgExit, relief=FLAT, bg=varColorIzqBarra, activebackground=varColorIzqBarra,
                  cursor="hand2", command=lambda: fun.cV(venConcep, venMain, flag)).place(x=10, y=650)
btnModiP = Button(venConcep, image=imgEdit, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                  cursor="hand2", command=lambda: fun.UpdatePro(lisPro, listEnProd, flag, btnModiP))
btnModiP.place(x=890, y=120)

# Ventana Cofiguracion #---------------------
btnAceptar = Button(venConfig, image=imgSuccess, relief=FLAT, bg=varColorFondo, activebackground=varColorFondo,
                    cursor="hand2",
                    command=lambda: fun.ConfiInicial(varServerCo, varUserCo, varPasswordCo, varDBCo, varUserLogCo,
                                                     varPassLogCo, varCorreoCo, varNombreCo, varRFCCo, varCalleCo,
                                                     varNoExCo, varColoniaCo, varLocalidadCo, varReferenciaCo,
                                                     varMunicipioCo, varEstadoCo, varPaisCo, varCPCo, varRegimenCo,
                                                     venConfig, venLogin, listVarDatEmi, flag)).place(x=934, y=640)

### ListBox ############################################################################################################
# Ventana Factura #-------------------------
lisFac = Listbox(venFactura, width="128", height="27", font="Courier 8")
lisFac.place(x=100, y=220)

# Ventana Clientes #------------------------
lisCli = Listbox(venClien, width="128", height="29", font="Courier 8")
lisCli.place(x=100, y=260)

# Ventana Concepto #------------------------
lisPro = Listbox(venConcep, width="128", height="32", font="Courier 8")
lisPro.place(x=100, y=220)

### ComboBox ###########################################################################################################
# Ventana Factura #-------------------------
comCli = ttk.Combobox(venFactura, width=15).place(x=330, y=110)
comPro = ttk.Combobox(venFactura, width=25).place(x=290, y=150)
comCan = ttk.Combobox(venFactura, values=varCantidad).place(x=560, y=150)

### Declaracion de funciones ###########################################################################################
fun.CompConfi(venLogin, venConfig, flag)
fun.DatosEmisor(listVarDatEmi)
fun.CarPro(lisPro)
fun.CarCli(lisCli)

venLogin.mainloop()
