from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import db
import ini


def cV(ventanaCerrar, ventanaAbrir, flag):
    ventanaCerrar.withdraw()
    ventanaAbrir.deiconify()


def CompConfi(venLogin, venConfig, flag):
    con = db.con()
    if con == 5:
        cV(venLogin, venConfig, flag)
    else:
        con.close()


def DatosEmisor(lista):
    listDatosEmi = ini.leer_iniEmIni()
    t = len(listDatosEmi)
    for i in range(0, t):
        lista[i].set(listDatosEmi[i])


def ConfiInicial(varServerCo, varUserCo, varPasswordCo, varDBCo, varUserLogCo, varPassLogCo, varCorreoCo, varNombreCo,
                 varRFCCo, varCalleCo, varNoExCo, varColoniaCo, varLocalidadCo, varReferenciaCo, varMunicipioCo,
                 varEstadoCo, varPaisCo, varCPCo, varRegimenCo, venConfig, venLogin, listVarDatEmi, flag):
    server = varServerCo.get()
    user = varUserCo.get()
    password = varPasswordCo.get()
    bd = varDBCo.get()
    correo = varCorreoCo.get()
    nombre = varNombreCo.get()
    rfc = varRFCCo.get()
    calle = varCalleCo.get()
    noEx = varNoExCo.get()
    colonia = varColoniaCo.get()
    localidad = varLocalidadCo.get()
    referencia = varReferenciaCo.get()
    municipio = varMunicipioCo.get()
    estado = varEstadoCo.get()
    pais = varPaisCo.get()
    cp = varCPCo.get()
    regimen = varRegimenCo.get()
    UserLog = varUserLogCo.get()
    PassLog = varPassLogCo.get()
    ini.escritura_iniBD(server, user, password, bd)
    ini.escritura_iniEM(correo, nombre, rfc, calle, noEx, colonia, localidad, referencia, municipio, estado, pais, cp,
                        regimen)
    db.InsertLogin(UserLog, PassLog)
    DatosEmisor(listVarDatEmi)
    cV(venConfig, venLogin, flag)


def ValidarLogin(vLogin, vMain, us, pa, flag):
    usu = us.get()
    pas = pa.get()
    consulta = db.Login(usu, pas)
    if consulta == []:
        messagebox.showwarning("Advertencia", "Usuario o Contraseña incorrectos")
    else:
        us.set("")
        pa.set("")
        cV(vLogin, vMain, flag)


def CarPro(lisPro):
    productos = db.CargPro()
    lisPro.delete(0, END)
    for producto in productos:
        lisPro.insert(END, " {0:11} {1:30} {2:70} {3:>12}".format(str(producto[0]), str(producto[1]), str(producto[3]),
                                                                  "$ " + str(producto[2])))


def NuevoPro(lisPro, lisen, flag, btn):
    if flag.get() == 0:
        id = lisen[0].get()
        nom = lisen[1].get()
        desc = lisen[2].get()
        uni = lisen[3].get()
        pre = lisen[4].get()
        res = db.InsertConcepto(id, nom, pre, desc, uni)
        if res == 1:
            messagebox.showinfo("exitoso", "Se agrego correctamente el producto")
            CarPro(lisPro)
            for i in range(0, 5):
                lisen[i].delete(0, END)
        else:
            messagebox.showwarning("Advertencia", "No se pudo crear el producto\n" + res)
    if flag.get() == 1:
        lisen[0].config(state=NORMAL)
        btn.config(state=NORMAL)
        id = lisen[0].get()
        nom = lisen[1].get()
        desc = lisen[2].get()
        uni = lisen[3].get()
        pre = lisen[4].get()
        res = db.UpdateConcepto(id, nom, pre, desc, uni)
        if res == 1:
            messagebox.showinfo("exitoso", "Se modifico correctamente el producto")
            CarPro(lisPro)
            for i in range(0, 5):
                lisen[i].delete(0, END)
            flag.set(0)
        else:
            messagebox.showwarning("Advertencia", "No se pudo modificar el producto\n" + res)


def EliminarPro(lisPro, lisen, flag, btn):
    if flag.get() == 0:
        productos = db.CargPro()
        index = lisPro.index(ACTIVE)
        id = productos[index][0]
        res = db.DeleteConcepto(id)
        if res == 1:
            messagebox.showinfo("exitoso", "Se borró correctamente el producto")
            CarPro(lisPro)
        else:
            messagebox.showwarning("Advertencia", "No se pudo borrar el producto\n" + res)
    if flag.get() == 1:
        lisen[0].config(state=NORMAL)
        btn.config(state=NORMAL)
        for i in range(0, 5):
            lisen[i].delete(0, END)
        flag.set(0)


def UpdatePro(lisPro, lisen, flag, btn):
    if flag.get() == 0:
        productos = db.CargPro()
        index = lisPro.index(ACTIVE)
        lisen[0].insert(END, productos[index][0])
        lisen[0].config(state=DISABLED)
        lisen[1].insert(END, productos[index][1])
        lisen[2].insert(END, productos[index][3])
        lisen[3].insert(END, productos[index][4])
        lisen[4].insert(END, productos[index][2])
        btn.config(state=DISABLED)
        flag.set(1)


########################################################################################################################
def CarCli(lisCli):
    clientes = db.CargCli()
    lisCli.delete(0, END)


# for cliente in clientes:
# lisCli.insert(END, " {0:11} {1:30} {2:70} {3:>12}".format(str(producto[0]), str(producto[1]), str(producto[3]), "$ " + str(producto[2])))

def NuevoCli(lisCli, lisen, flag, btn):
    if flag.get() == 0:
        id = lisen[0].get()
        nom = lisen[1].get()
        desc = lisen[2].get()
        uni = lisen[3].get()
        pre = lisen[4].get()
        res = db.InsertConcepto(id, nom, pre, desc, uni)
        if res == 1:
            messagebox.showinfo("exitoso", "Se agrego correctamente el producto")
            CarPro(lisCli)
            for i in range(0, 5):
                lisen[i].delete(0, END)
        else:
            messagebox.showwarning("Advertencia", "No se pudo crear el producto\n" + res)
    if flag.get() == 1:
        lisen[0].config(state=NORMAL)
        btn.config(state=NORMAL)
        id = lisen[0].get()
        nom = lisen[1].get()
        desc = lisen[2].get()
        uni = lisen[3].get()
        pre = lisen[4].get()
        res = db.UpdateConcepto(id, nom, pre, desc, uni)
        if res == 1:
            messagebox.showinfo("exitoso", "Se modifico correctamente el producto")
            CarPro(lisCli)
            for i in range(0, 5):
                lisen[i].delete(0, END)
            flag.set(0)
        else:
            messagebox.showwarning("Advertencia", "No se pudo modificar el producto\n" + res)


def EliminarCli(lisCli, lisen, flag, btn):
    if flag.get() == 0:
        productos = db.CargPro()
        index = lisCli.index(ACTIVE)
        id = productos[index][0]
        res = db.DeleteConcepto(id)
        if res == 1:
            messagebox.showinfo("exitoso", "Se borró correctamente el producto")
            CarPro(lisCli)
        else:
            messagebox.showwarning("Advertencia", "No se pudo borrar el producto\n" + res)
    if flag.get() == 1:
        lisen[0].config(state=NORMAL)
        btn.config(state=NORMAL)
        for i in range(0, 5):
            lisen[i].delete(0, END)
        flag.set(0)


def UpdateCli(lisCli, lisen, flag, btn):
    if flag.get() == 0:
        productos = db.CargPro()
        index = lisCli.index(ACTIVE)
        lisen[0].insert(END, productos[index][0])
        lisen[0].config(state=DISABLED)
        lisen[1].insert(END, productos[index][1])
        lisen[2].insert(END, productos[index][3])
        lisen[3].insert(END, productos[index][4])
        lisen[4].insert(END, productos[index][2])
        btn.config(state=DISABLED)
        flag.set(1)
