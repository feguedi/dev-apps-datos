import psycopg2
import ini


#Funcion para la conexion a la base de datos
def con():
    datos = ini.leer_iniBD()
    Server = datos[0]
    Usuariio = datos[1]
    Password = datos[2]
    DB = datos[3]
    try:
        conexion = psycopg2.connect("dbname='" + DB + "' user='" + Usuariio + "' host='" + Server + "' password='" + Password + "'")
        return conexion
    except:
        return 5


def InsertLogin(User, Pass):
    conn = con()
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO login VALUES('" + str(User) + "', '" + str(Pass) + "');")
        conn.commit()
        conn.close()
    except psycopg2.Error as e:
        conn.close()
        return e.pgerror
    except:
        conn.close()
        return "Error desconocido"


def InsertConcepto(IdCon, Nombre, Precio, Descripcion, Unidad):
    conn = con()
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO producto VALUES('" + str(IdCon) + "', '" + str(Nombre) + "','" +Precio +"','" + str(Descripcion) + "', '" + str(Unidad) + "');")
        conn.commit()
        conn.close()
        return 1
    except psycopg2.Error as e:
        conn.close()
        return e.pgerror
    except:
        conn.close()
        return "Error desconocido"


def Login(us, pa):
    conn = con()
    cur = conn.cursor()
    cur.execute("SELECT * FROM login WHERE logUsuario = '" + us + "'AND logPassword = '" + pa + "';")
    consulta = cur.fetchall()
    conn.close()
    return consulta


def CargPro():
    conn = con()
    cur = conn.cursor()
    cur.execute("SELECT * FROM producto")
    consulta = cur.fetchall()
    conn.close()
    return consulta


def DeleteConcepto(id):
    conn = con()
    cur = conn.cursor()
    try:
        cur.execute("DELETE FROM producto Where pronoidentificacion = '" + id + "'")
        conn.commit()
        conn.close()
        return 1
    except psycopg2.Error as e:
        conn.close()
        return e.pgerror
    except:
        conn.close()
        return "Error desconocido"


def UpdateConcepto(IdCon, Nombre, Precio, Descripcion, Unidad):
    conn = con()
    cur = conn.cursor()
    try:
        cur.execute("UPDATE producto SET pronombre='" + Nombre + "', proprecio=" + Precio + ", prodescripcion='" + Descripcion +"', prounidad='" + Unidad + "' WHERE pronoidentificacion = '" + IdCon + "'")
        conn.commit()
        conn.close()
        return 1
    except psycopg2.Error as e:
        conn.close()
        return e.pgerror
    except:
        conn.close()
        return "Error desconocido"


def CargCli():
    conn = con()
    cur = conn.cursor()
    cur.execute("SELECT * FROM receptor r, domicilio d Where r.recrfc = d.recrfc")
    consulta = cur.fetchall()
    conn.close()
    return consulta
