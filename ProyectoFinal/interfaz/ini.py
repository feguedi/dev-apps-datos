import configparser

cfg = configparser.ConfigParser()
cfg.read(["configuracion.ini"])


def escritura_iniBD(server, user, password, bd):
    cfg.set("postgres", "server", server)
    cfg.set("postgres", "user", user)
    cfg.set("postgres", "password", password)
    cfg.set("postgres", "db", bd)
    f = open("configuracion.ini", "w")
    cfg.write(f)
    f.close()


def escritura_iniEM(correo, nombre, rfc, calle, noEx, colonia, localidad, referencia, municipio, estado, pais, cp,
                    regimen):
    cfg.set("empresa", "correo", correo)
    cfg.set("empresa", "nombre", nombre)
    cfg.set("empresa", "rfc", rfc)
    cfg.set("empresa", "calle", calle)
    cfg.set("empresa", "noExterior", noEx)
    cfg.set("empresa", "colonia", colonia)
    cfg.set("empresa", "localidad", localidad)
    cfg.set("empresa", "referencia", referencia)
    cfg.set("empresa", "municipio", municipio)
    cfg.set("empresa", "estado", estado)
    cfg.set("empresa", "pais", pais)
    cfg.set("empresa", "cp", cp)
    cfg.set("empresa", "regimen", regimen)
    f = open("configuracion.ini", "w")
    cfg.write(f)
    f.close()


def leer_iniBD():
    if not cfg.read(["configuracion.ini"]):
        print("No existe el archivo")
    server = cfg.get("postgres", "server")
    usuario = cfg.get("postgres", "user")
    password = cfg.get("postgres", "password")
    db = cfg.get("postgres", "db")
    bd = [server, usuario, password, db]
    return bd


def leer_iniEM():
    if not cfg.read(["configuracion.ini"]):
        print("No existe el archivo")
    correo = cfg.get("empresa", "correo")
    nombre = cfg.get("empresa", "nombre")
    rfc = cfg.get("empresa", "rfc")
    calle = cfg.get("empresa", "calle")
    noEx = cfg.get("empresa", "noExterior")
    colonia = cfg.get("empresa", "colonia")
    localidad = cfg.get("empresa", "localidad")
    municipio = cfg.get("empresa", "municipio")
    estado = cfg.get("empresa", "estado")
    pais = cfg.get("empresa", "pais")
    cp = cfg.get("empresa", "cp")
    domicilio = calle + " " + noEx + " " + colonia + " " + localidad + " " + municipio + " " + estado + " " + pais + " CP. " + cp
    regimen = cfg.get("empresa", "regimen")
    empresa = [correo, nombre, rfc, domicilio, regimen]
    return empresa


def leer_iniEmIni():
    if not cfg.read(["configuracion.ini"]):
        print("No existe el archivo")
    correo = cfg.get("empresa", "correo")
    nombre = cfg.get("empresa", "nombre")
    rfc = cfg.get("empresa", "rfc")
    calle = cfg.get("empresa", "calle")
    noEx = cfg.get("empresa", "noExterior")
    colonia = cfg.get("empresa", "colonia")
    localidad = cfg.get("empresa", "localidad")
    referencia = cfg.get("empresa", "referencia")
    municipio = cfg.get("empresa", "municipio")
    estado = cfg.get("empresa", "estado")
    pais = cfg.get("empresa", "pais")
    cp = cfg.get("empresa", "cp")
    regimen = cfg.get("empresa", "regimen")
    empresa = [correo, nombre, rfc, calle, noEx, colonia, localidad, referencia, municipio, estado, pais, cp, regimen]
    return empresa
