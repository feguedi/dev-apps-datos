# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../registrobd.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QWidget, QApplication

demografia = {
    'Aguascalientes': ['Aguascalientes', 'Asientos', 'Calvillo', 'Cosío', 'Jesús María', 'Pabellón de Arteaga',
                       'Rincón de Romos', 'San José de Gracia', 'Tepezalá', 'El Llano', 'San Francisco de los Romo'],
    'Baja California': ['Ensenada', 'Mexicali', 'Tecate', 'Tijuana', 'Playas de Rosarito'],
    'Baja California Sur': ['Comondú', 'Mulegé', 'La Paz', 'Los Cabos', 'Loreto'],
    'Campeche': ['Calkiní', 'Campeche', 'Carmen', 'Champotón', 'Hecelchakán', 'Hopelchén', 'Palizada', 'Tenabo', 
                 'Escárcega', 'Calakmul', 'Candelaria'],
    'Coahuila de Zaragoza': ['Abasolo', 'Acuña', 'Allende', 'Arteaga', 'Candela', 'Castaños', 'Cuatro Ciénegas', 
                             'Escobedo', 'Francisco I. Madero', 'Frontera', 'General Cepeda', 'Guerrero', 'Hidalgo',
                             'Jiménez', 'Juárez', 'Lamadrid', 'Matamoros', 'Monclova', 'Morelos', 'Múzquiz', 'Nadadores'
                             'Nava', 'Ocampo', 'Parras', 'Piedras Negras', 'Progreso', 'Ramos Arizpe', 'Sabinas', 
                             'Sacramento', 'Saltillo', 'San Buenaventura', 'San Juan de Sabinas', 'San Pedro',
                             'Sierra Mojada', 'Torreón', 'Viesca', 'Villa Unión', 'Zaragoza'],
    'Colima': ['Armería', 'Colima', 'Comala', 'Coquiamatlán', 'Cuauhtémoc', 'Ixtlahuacán', 'Manzanillo', 'Minatitlán',
               'Tecomán', 'Villa de Álvarez'],
    'Chiapas': ["Acacoyagua", "Acala", "Acapetahua", "Altamirano", "Amatán" ,"Amatenango de la Frontera", 
                "Amatenango del Valle", "Angel Albino Corzo", "Arriaga", "Bejucal de Ocampo", "Bella Vista", "Berriozábal",
                "Bochil", "El Bosque", "Cacahoatán", "Catazajá", "Cintalapa", "Coapilla", "Comitán de Domínguez", 
                "La Concordia", "Copainalá", "Chalchihuitán", "Chamula", "Chanal", "Chapultenango", "Chenalhó", 
                "Chiapa de Corzo", "Chiapilla", "Chicoasén", "Chicomuselo", "Chilón", "Escuintla", "Francisco León",
                "Frontera Comalapa", "Frontera Hidalgo", "La Grandeza", "Huehuetán", "Huixtán", "Huitiupán", "Huixtla",
                "La Independencia", "Ixhuatán", "Ixtacomitán", "Ixtapa", "Ixtapangajoya", "Jiquipilas", "Jitotol", 
                "Juárez", "Larráinzar", "La Libertad", "Mapastepec", "Las Margaritas", "Mazapa de Madero", "Mazatán", 
                "Metapa", "Mitontic", "Motozintla", "Nicolás Ruíz", "Ocosingo", "Ocotepec", "Ocozocoautla de Espinosa",
                "Ostuacán", "Osumacinta", "Oxchuc", "Palenque", "Pantelhó", "Pantepec", "Pichucalco", "Pijijiapan", 
                "El Porvenir", "Villa Comaltitlán", "Pueblo Nuevo Solistahuacán", "Rayón", "Reforma", "Las Rosas", 
                "Sabanilla", "Salto de Agua", "San Cristóbal de las Casas", "San Fernando", "Siltepec", "Simojovel", 
                "Sitalá", "Socoltenango", "Solosuchiapa", "Soyaló", "Suchiapa", "Suchiate", "Sunuapa", "Tapachula",
                "Tapalapa", "Tapilula", "Tecpatán", "Tenejapa", "Teopisca", "Tila", "Tonalá", "Totolapa", "La Trinitaria",
                "Tumbalá", "Tuxtla Gutiérrez", "Tuxtla Chico", "Tuzantán", "Tzimol", "Unión Juárez", "Venustiano Carranza",
                "Villa Corzo", "Villaflores", "Yajalón", "San Lucas", "Zinacantán", "San Juan Cancuc", "Aldama",
                "Benemérito de las Américas", "Maravilla Tenejapa", "Marqués de Comillas", "Montecristo de Guerrero", 
                "San Andrés Duraznal", "Santiago el Pinar"],
    'Chihuahua': ["Ahumada", "Aldama", "Allende", "Aquiles Serdán", "Ascensión", "Bachíniva", "Balleza", "Batopilas", 
                  "Bocoyna", "Buenaventura", "Camargo", "Carichí", "Casas Grandes", "Coronado", "Coyame del Sotol", 
                  "La Cruz", "Cuauhtémoc", "Cusihuiriachi", "Chihuahua", "Chínipas", "Delicias", "Dr. Belisario Domínguez", 
                  "Galeana", "Santa Isabel", "Gómez Farías", "Gran Morelos", "Guachochi", "Guadalupe", "Guadalupe y Calvo",
                  "Guazapares", "Guerrero", "Hidalgo del Parral", "Huejotitán", "Ignacio Zaragoza", "Janos", "Jiménez", 
                  "Juárez", "Julimes", "López", "Madera", "Maguarichi", "Manuel Benavides", "Matachí", "Matamoros",
                  "Meoqui", "Morelos", "Moris", "Namiquipa", "Nonoava", "Nuevo Casas Grandes", "Ocampo", "Ojinaga",
                  "Praxedis G. Guerrero", "Riva Palacio", "Rosales", "Rosario", "San Francisco de Borja",
                  "San Francisco de Conchos", "San Francisco del Oro", "Santa Bárbara", "Satevó", "Saucillo", "Temósachic",
                  "El Tule", "Urique", "Uruachi", "Valle de Zaragoza"],
    'Distrito Federal': ["Azcapotzalco", "Coyoacán", "Cuajimalpa de Morelos", "Gustavo A. Madero", "Iztacalco", "Iztapalapa",
                         "La Magdalena Contreras", "Milpa Alta", "Álvaro Obregón", "Tláhuac", "Tlalpan", "Xochimilco", 
                         "Benito Juárez", "Cuauhtémoc", "Miguel Hidalgo", "Venustiano Carranza"],
    'Durango': ["Canatlán", "Canelas", "Coneto de Comonfort", "Cuencamé", "Durango", "General Simón Bolívar", "Gómez Palacio", 
                "Guadalupe Victoria", "Guanaceví", "Hidalgo", "Indé", "Lerdo", "Mapimí", "Mezquital", "Nazas", "Nombre de Dios",
                "Ocampo", "El Oro", "Otáez", "Pánuco de Coronado", "Peñón Blanco", "Poanas", "Pueblo Nuevo", "Rodeo",
                "San Bernardo", "San Dimas", "San Juan de Guadalupe", "San Juan del Río", "San Luis del Cordero",
                "San Pedro del Gallo", "Santa Clara", "Santiago Papasquiaro", "Súchil", "Tamazula", "Tepehuanes", "Tlahualilo",
                "Topia", "Vicente Guerrero", "Nuevo Ideal"],
    'Guanajuato': [
                   "Abasolo", "Acámbaro", "San Miguel de Allende", "Apaseo el Alto", "Apaseo el Grande", "Atarjea", "Celaya", 
                   "Manuel Doblado", "Comonfort", "Coroneo", "Cortazar", "Cuerámaro", "Doctor Mora",
                   "Dolores Hidalgo Cuna de la Independencia Nacional","Guanajuato", "Huanímaro", "Irapuato", "Jaral del Progreso",
                   "Jerécuaro", "León", "Moroleón", "Ocampo", "Pénjamo", "Pueblo Nuevo", "Purísima del Rincón", "Romita", "Salamanca",
                   "Salvatierra", "San Diego de la Unión", "San Felipe", "San Francisco del Rincón", "San José Iturbide",
                   "San Luis de la Paz", "Santa Catarina", "Santa Cruz de Juventino Rosas", "Santiago Maravatío", "Silao de la Victoria",
                   "Tarandacuao", "Tarimoro", "Tierra Blanca", "Uriangato", "Valle de Santiago", "Victoria", "Villagrán", "Xichú",
                   "Yuriria"],
    'Guerrero': [
                 "Acapulco de Juárez","Ahuacuotzingo","Ajuchitlán del Progreso","Alcozauca de Guerrero","0001","Alcozauca de Guerrero",
                 "Alpoyeca","Apaxtla","Arcelia","Atenango del Río","Atlamajalcingo del Monte","Atlixtac","Atoyac de Álvarez",
                 "Ayutla de los Libres","Azoyú","Benito Juárez","Buenavista de Cuéllar","Coahuayutla de José María Izazaga","Cocula",
                 "Copala","Copalillo","Copanatoyac","Coyuca de Benítez","Coyuca de Catalán","Cuajinicuilapa","Cualác","Cuautepec",
                 "Cuetzala del Progreso","Cutzamala de Pinzón","Chilapa de Álvarez","Chilpancingo de los Bravo","Florencio Villarreal",
                 "General Canuto A. Neri","General Heliodoro Castillo","Huamuxtitlán","Huitzuco de los Figueroa","Iguala de la Independencia",
                 "Igualapa","Ixcateopan de Cuauhtémoc","0001","Zihuatanejo de Azueta","Juan R. Escudero","Leonardo Bravo","Malinaltepec",
                 "Mártir de Cuilapan","Metlatónoc","Mochitlán","Olinalá","Ometepec","Pedro Ascencio Alquisiras","Petatlán","Pilcaya",
                 "Pungarabato","Quechultenango","San Luis Acatlán","San Marcos","San Miguel Totolapan","Taxco de Alarcón","Tecoanapa",
                 "Técpan de Galeana","Teloloapan","Tepecoacuilco de Trujano","Tetipac","Tixtla de Guerrero","Tlacoachistlahuaca","Tlacoapa",
                 "Tlalchapa","Tlalixtaquilla de Maldonado","Tlapa de Comonfort","Tlapehuala","La Unión de Isidoro Montes de Oca","Xalpatláhuac",
                 "Xochihuehuetlán","Xochistlahuaca","Zapotitlán Tablas","Zirándaro","Zitlala","Eduardo Neri","Acatepec","Marquelia",
                 "Cochoapa el Grande","José Joaquín de Herrera","Juchitán","Iliatenco"],
    'Hidalgo': [
                "Acatlán","Acaxochitlán","Actopan","Agua Blanca de Iturbide","Ajacuba","Alfajayucan","Almoloya","Apan","El Arenal","Atitalaquia",
                "Atlapexco","Atotonilco el Grande","Atotonilco de Tula","Calnali","Cardonal","Cuautepec de Hinojosa","Chapantongo","Chapulhuacán",
                "Chilcuautla","Eloxochitlán","Emiliano Zapata","Epazoyucan","Francisco I. Madero","Huasca de Ocampo","Huautla","Huazalingo",
                "Huehuetla","Huejutla de Reyes","Huichapan","Ixmiquilpan","Jacala de Ledezma","Jaltocán","Juárez Hidalgo","Lolotla","Metepec",
                "San Agustín Metzquititlán","Metztitlán","Mineral del Chico","Mineral del Monte","La Misión","Mixquiahuala de Juárez",
                "Molango de Escamilla", "Nicolás Flores","Nopala de Villagrán","Omitlán de Juárez","San Felipe Orizatlán","Pacula",
                "Pachuca de Soto","Pisaflores","Progreso de Obregón","Mineral de la Reforma","San Agustín Tlaxiaca","San Bartolo Tutotepec",
                "San Salvador","Santiago de Anaya","Santiago Tulantepec de Lugo""Singuilucan","Tasquillo","Tecozautla","Tenango de Doria",
                "Tepeapulco","Tepehuacán de Guerrero","Tepeji del Río de Ocampo","Tepetitlán","Tetepango","Villa de Tezontepec",
                "Tezontepec de Aldama","Tianguistengo","Tizayuca","Tlahuelilpan","Tlahuiltepa","Tlanalapa","Tlanchinol","Tlaxcoapan","Tolcayuca",
                "Tula de Allende","Tulancingo de Bravo","Xochiatipan","Xochicoatlán","Yahualica","Zacualtipán de Ángeles","Zapotlán de Juárez",
                "Zempoala","Zimapán"],
    'Jalisco': [
                "Acatic","Acatlán de Juárez","Ahualulco de Mercado","Amacueca","Amatitán","Ameca","San Juanito de Escobedo","Arandas","El Arenal",
                "Atemajac de Brizuela","Atengo","Atenguillo","Atotonilco el Alto","Atoyac","Autlán de Navarro","Ayotlán","Ayutla","La Barca","Bolaños",
                "Cabo Corrientes","Casimiro Castillo","Cihuatlán","Zapotlán el Grande","Cocula","Colotlán","Concepción de Buenos Aires",
                "Cuautitlán de García Barragán","Cuautla","Cuquío","Chapala","Chimaltitán","Chiquilistlán","Degollado","Ejutla","Encarnación de Díaz",
                "Etzatlán","El Grullo","Guachinango","Guadalajara","Hostotipaquillo","Huejúcar","Huejuquilla el Alto","La Huerta",
                "Ixtlahuacán de los Membrillos","Ixtlahuacán del Río","Jalostotitlán","Jamay","Jesús María","Jilotlán de los Dolores","Jocotepec",
                "Juanacatlán","Juchitlán","Lagos de Moreno","El Limón","Magdalena","La Manzanilla de la Paz","Mascota","Mazamitla","Mexticacán",
                "Mezquitic","Mixtlán","Ocotlán","Ojuelos de Jalisco","Pihuamo","Poncitlán","Puerto Vallarta","Villa Purificación","Quitupan",
                "El Salto","San Cristóbal de la Barranca","San Diego de Alejandría","San Juan de los Lagos","San Julián","San Marcos",
                "San Martín de Bolaños","San Martín Hidalgo","San Miguel el Alto","Gómez Farías","San Sebastián del Oeste","Santa María de los Ángeles",
                "Sayula","Tala","Talpa de Allende","Tamazula de Gordiano","Tapalpa","Tecalitlán","Tecolotlán","Techaluta de Montenegro","Tenamaxtlán",
                "Teocaltiche","Teocuitatlán de Corona","Tepatitlán de Morelos","Tequila","Teuchitlán","Tizapán el Alto","Tlajomulco de Zúñiga",
                "San Pedro Tlaquepaque","Tolimán","Tomatlán","Tonalá","Tonaya","Tonila","Totatiche","Tototlán","Tuxcacuesco","Tuxcueca","Tuxpan",
                "Unión de San Antonio","Unión de Tula","Valle de Guadalupe","Valle de Juárez","San Gabriel","Villa Corona","Villa Guerrero",
                "Villa Hidalgo","Cañadas de Obregón","Yahualica de González Gallo","Zacoalco de Torres","Zapopan","Zapotiltic","Zapotitlán de Vadillo",
                "Zapotlán del Rey","Zapotlanejo","San Ignacio Cerro Gordo",],    
    'México': [
               "Acambay de Ruíz Castañeda","Acolman","Aculco","Almoloya de Alquisiras","Almoloya de Juárez","Almoloya del Río","Amanalco","Amatepec","Amecameca",
               "Apaxco","Atenco","Atizapán","Atizapán de Zaragoza","Atlacomulco","Atlautla","Axapusco","Ayapango","Calimaya","Capulhuac","Coacalco de Berriozábal",
               "Coatepec Harinas","Cocotitlán","Coyotepec","Cuautitlán","Chalco","Chapa de Mota","Chapultepec","Chiautla","Chicoloapan","Chiconcuac","Chimalhuacán",
               "Donato Guerra","Ecatepec de Morelos","Ecatzingo","Huehuetoca","Hueypoxtla","Huixquilucan","Isidro Fabela","Ixtapaluca","Ixtapan de la Sal",
               "Ixtapan del Oro","Ixtlahuaca","Xalatlaco","Jaltenco","Jilotepec","Jilotzingo","Jiquipilco","Jocotitlán","Joquicingo","Juchitepec","Lerma","Malinalco",
               "Melchor Ocampo","Metepec","Mexicaltzingo","Morelos","Naucalpan de Juárez","Nezahualcóyotl","Nextlalpan","Nicolás Romero","Nopaltepec","Ocoyoacac",
               "Ocuilan","El Oro","Otumba","Otzoloapan","Otzolotepec","Ozumba","Papalotla","La Paz","Polotitlán","Rayón","San Antonio la Isla",
               "San Felipe del Progreso","San Martín de las Pirámides","San Mateo Atenco","San Simón de Guerrero","Santo Tomás","Soyaniquilpan de Juárez","Sultepec",
               "Tecámac","Tejupilco","Temamatla","Temascalapa","Temascalcingo","Temascaltepec","Temoaya","Tenancingo","Tenango del Aire","Tenango del Valle",
               "Teoloyucan","Teotihuacán","Tepetlaoxtoc","Tepetlixpa","Tepotzotlán","Tequixquiac","Texcaltitlán","Texcalyacac","Texcoco","Tezoyuca","Tianguistenco",
               "Timilpan","Tlalmanalco","Tlalnepantla de Baz","Tlatlaya","Toluca","Tonatico","Tultepec","Tultitlán","Valle de Bravo","Villa de Allende",
               "Villa del Carbón","Villa Guerrero","Villa Victoria","Xonacatlán","Zacazonapan","Zacualpan","Zinacantepec","Zumpahuacán","Zumpango",
               "Cuautitlán Izcalli","Valle de Chalco Solidaridad","Luvianos","San José del Rincón","Tonanitla",],
    'Michoacán de Ocampo': [
                            "Acuitzio","Aguililla","Álvaro Obregón","Angamacutiro","Angangueo","Apatzingán","Aporo","Aquila","Ario","Arteaga","Briseñas",
                            "Buenavista","Carácuaro","Coahuayana","Coalcomán de Vázquez Pallares","Coeneo","Contepec","Copándaro","Cotija","Cuitzeo","Charapan",
                            "Charo","Chavinda","Cherán","Chilchota","Chinicuila","Chucándiro","Churintzio","Churumuco","Ecuandureo","Epitacio Huerta",
                            "Erongarícuaro","Gabriel Zamora","Hidalgo","La Huacana","Huandacareo","Huaniqueo","Huetamo","Huiramba","Indaparapeo","Irimbo","Ixtlán",
                            "Jacona","Jiménez","Jiquilpan","Juárez","Jungapeo","Lagunillas","Madero","Maravatío","Marcos Castellanos","Lázaro Cárdenas",
                            "Morelia","Morelos","Múgica","Nahuatzen","Nocupétaro","Nuevo Parangaricutiro","Nuevo Urecho","Numarán","Ocampo","Pajacuarán","Panindícuaro",
                            "Parácuaro","Paracho","Pátzcuaro","Penjamillo","Peribán","La Piedad","Purépero","Puruándiro","Queréndaro","Quiroga","Cojumatlán de Régules",
                            "Los Reyes","Sahuayo","San Lucas","Santa Ana Maya","Salvador Escalante","Senguio","Susupuato","Tacámbaro","Tancítaro","Tangamandapio",
                            "Tangancícuaro","Tanhuato","Taretan","Tarímbaro","Tepalcatepec","Tingambato","Tingüindín","Tiquicheo de Nicolás Romero","Tlalpujahua",
                            "Tlazazalca","Tocumbo","Tumbiscatío","Turicato","Tuxpan","Tuzantla","Tzintzuntzan","Tzitzio","Uruapan","Venustiano Carranza","Villamar",
                            "Vista Hermosa","Yurécuaro","Zacapu","Zamora","Zináparo","Zinapécuaro","Ziracuaretiro","Zitácuaro","José Sixto Verduzco",],
    'Morelos': [
                "Amacuzac","Atlatlahucan","Axochiapan","Ayala","Coatlán del Río","Cuautla","Cuernavaca","Emiliano Zapata","Huitzilac","Jantetelco",
                "Jiutepec","Jojutla","Jonacatepec","Mazatepec","Miacatlán","Ocuituco","Puente de Ixtla","Temixco","Tepalcingo","Tepoztlán","Tetecala",
                "Tetela del Volcán","Tlalnepantla","Tlaltizapán de Zapata","Tlaquiltenango","Tlayacapan","Totolapan","Xochitepec","Yautepec","Yecapixtla",
                "Zacatepec","Zacualpan de Amilpas","Temoac",],
    'Nayarit': [
                "Acaponeta","Ahuacatlán","Amatlán de Cañas","Compostela","Huajicori","Ixtlán del Río","Jala","Xalisco","Del Nayar","Rosamorada","Ruíz",
                "San Blas","San Pedro Lagunillas","Santa María del Oro","Santiago Ixcuintla","Tecuala","Tepic","Tuxpan","La Yesca","Bahía de Banderas",],
    'Nuevo León': [
                   "Abasolo","Agualeguas","Los Aldamas","Allende","Anáhuac","Apodaca","Aramberri","Bustamante","Cadereyta Jiménez","El Carmen","Cerralvo",
                   "Ciénega de Flores","China","Doctor Arroyo","Doctor Coss","Doctor González","Galeana","García","San Pedro Garza García","General Bravo",
                   "General Escobedo","General Terán","General Treviño","General Zaragoza","General Zuazua","Guadalupe","Los Herreras","Higueras","Hualahuises",
                   "Iturbide","Juárez","Lampazos de Naranjo","Linares","Marín","Melchor Ocampo","Mier y Noriega","Mina","Montemorelos","Monterrey","Parás",
                   "Pesquería","Los Ramones","Rayones","Sabinas Hidalgo","Salinas Victoria","San Nicolás de los Garza","Hidalgo","Santa Catarina","Santiago",
                   "Vallecillo","Villaldama",],
    'Oaxaca de Juárez': [
                         ],
    'Puebla': [
               "Acajete","Acateno","Acatlán","Acatzingo","Acteopan","Ahuacatlán","Ahuatlán","Ahuazotepec","Ahuehuetitla","Ajalpan","Albino Zertuche","Aljojuca",
               "Altepexi","Amixtlán","Amozoc","Aquixtla","Atempan","Atexcal","Atlixco","Atoyatempan","Atzala","Atzitzihuacán","Atzitzintla","Axutla",
               "Ayotoxco de Guerrero","Calpan","Caltepec","Camocuautla","Caxhuacan","Coatepec","Coatzingo","Cohetzala","Cohuecan","Coronango","Coxcatlán","Coyomeapan",
               "Coyotepec","Cuapiaxtla de Madero","Cuautempan","Cuautinchán","Cuautlancingo","Cuayuca de Andrade","Cuetzalan del Progreso","Cuyoaco",
               "Chalchicomula de Sesma","Chapulco","Chiautla","Chiautzingo","Chiconcuautla","Chichiquila","Chietla","Chigmecatitlán","Chignahuapan","Chignautla","Chila",
               "Chila de la Sal","Honey","Chilchotla","Chinantla","Domingo Arenas","Eloxochitlán","Epatlán","Esperanza","Francisco Z. Mena","General Felipe Ángeles",
               "Guadalupe","Guadalupe Victoria","Hermenegildo Galeana","Huaquechula","Huatlatlauca","Huauchinango","Huehuetla","Huehuetlán el Chico","Huejotzingo",
               "Hueyapan","Hueytamalco","Hueytlalpan","Huitzilan de Serdán","Huitziltepec","Atlequizayan","Ixcamilpa de Guerrero","Ixcaquixtla","Ixtacamaxtitlán","Ixtepec",
               "Izúcar de Matamoros","Jalpan","Jolalpan","Jonotla","Jopala","Juan C. Bonilla","Juan Galindo","Juan N. Méndez","Lafragua","Libres",
               "La Magdalena Tlatlauquitepec","Mazapiltepec de Juárez","Mixtla","Molcaxac","Cañada Morelos","Naupan","Nauzontla","Nealtican","Nicolás Bravo","Nopalucan",
               "Ocotepec","Ocoyucan","Olintla","Oriental","Pahuatlán","Palmar de Bravo","Pantepec","Petlalcingo","Piaxtla","Puebla","Quecholac","Quimixtlán",
               "Rafael Lara Grajales","Los Reyes de Juárez","San Andrés Cholula","San Antonio Cañada","San Diego la Mesa Tochimiltzingo","San Felipe Teotlalcingo",
               "San Felipe Tepatlán","San Gabriel Chilac","San Gregorio Atzompa","San Jerónimo Tecuanipan","San Jerónimo Xayacatlán","San José Chiapa","San José Miahuatlán",
               "San Juan Atenco","San Juan Atzompa","San Martín Texmelucan","San Martín Totoltepec","San Matías Tlalancaleca","San Miguel Ixitlán","San Miguel Xoxtla",
               "San Nicolás Buenos Aires","San Nicolás de los Ranchos","San Pablo Anicano","San Pedro Cholula","San Pedro Yeloixtlahuaca","San Salvador el Seco",
               "San Salvador el Verde","San Salvador Huixcolotla","San Sebastián Tlacotepec","Santa Catarina Tlaltempan","Santa Inés Ahuatempan","Santa Isabel Cholula",
               "Santiago Miahuatlán","Huehuetlán el Grande","Santo Tomás Hueyotlipan","Soltepec","Tecali de Herrera","Tecamachalco","Tecomatlán","Tehuacán","Tehuitzingo",
               "Tenampulco","Teopantlán","Teotlalco","Tepanco de López","Tepango de Rodríguez","Tepatlaxco de Hidalgo","Tepeaca","Tepemaxalco","Tepeojuma","Tepetzintla",
               "Tepexco","Tepexi de Rodríguez","Tepeyahualco","Tepeyahualco de Cuauhtémoc","Tetela de Ocampo","Teteles de Avila Castillo","Teziutlán","Tianguismanalco",
               "Tilapa","Tlacotepec de Benito Juárez","Tlacuilotepec","Tlachichuca","Tlahuapan","Tlaltenango","Tlanepantla","Tlaola","Tlapacoya","Tlapanalá",
               "Tlatlauquitepec","Tlaxco","Tochimilco","Tochtepec","Totoltepec de Guerrero","Tulcingo","Tuzamapan de Galeana","Tzicatlacoyan","Venustiano Carranza",
               "Vicente Guerrero","Xayacatlán de Bravo","Xicotepec","Xicotlán","Xiutetelco","Xochiapulco","Xochiltepec","Xochitlán de Vicente Suárez",
               "Xochitlán Todos Santos","Yaonáhuac","Yehualtepec","Zacapala","Zacapoaxtla","Zacatlán","Zapotitlán","Zapotitlán de Méndez","Zaragoza","Zautla","Zihuateutla",
               "Zinacatepec","Zongozotla","Zoquiapan","Zoquitlán",],
    'Querétaro': [
                  "Amealco de Bonfil","Pinal de Amoles","Arroyo Seco","Cadereyta de Montes","Colón","Colón","Corregidora","Ezequiel Montes","Huimilpan",
                  "Jalpan de Serra","Landa de Matamoros","El Marqués","0001","Pedro Escobedo","Peñamiller","Querétaro","San Joaquín","San Juan del Río",
                  "Tequisquiapan","Tolimán",],
    'Quintana Roo': [
                     "Cozumel","Felipe Carrillo Puerto","Isla Mujeres","Othón P. Blanco","Benito Juárez","José María Morelos","Lázaro Cárdenas","Solidaridad","Tulum",
                     "Bacalar",
    'San Luis Potosí': [
                        "Ahualulco","Alaquines","Aquismón","Armadillo de los Infante","Cárdenas","Catorce","Cedral","Cerritos","Cerro de San Pedro","Ciudad del Maíz",
                        "Ciudad Fernández","Tancanhuitz","Ciudad Valles","Coxcatlán","Charcas","Ebano","Guadalcázar","Huehuetlán","Lagunillas","Matehuala",
                        "Mexquitic de Carmona","Moctezuma","Rayón","Rioverde","Salinas","San Antonio","San Ciro de Acosta","San Luis Potosí","San Martín Chalchicuautla",
                        "San Nicolás Tolentino","Santa Catarina","Santa María del Río","Santo Domingo","San Vicente Tancuayalab","Soledad de Graciano Sánchez","Tamasopo",
                        "Tamazunchale","Tampacán","Tampamolón Corona","Tamuín","Tanlajás","Tanquián de Escobedo","Tierra Nueva","Vanegas","Venado","Villa de Arriaga",
                        "Villa de Guadalupe","Villa de la Paz","Villa de Ramos","Villa de Reyes","Villa Hidalgo","Villa Juárez","Axtla de Terrazas","Xilitla","Zaragoza",
                        "Villa de Arista","Matlapa","El Naranjo"],
    'Sinaloa': [
                "Ahome","Angostura","Badiraguato","Concordia","Cosalá","Culiacán","Choix","Elota","Escuinapa","El Fuerte","Guasave","Mazatlán","Mocorito","Rosario",
                "Salvador Alvarado","San Ignacio","Sinaloa","Navolato"]
    'Sonora': [
               "Aconchi",
               "Agua Prieta","Alamos","Altar","Arivechi","Arizpe","Atil","Bacadéhuachi","Bacanora","Bacerac","Bacoachi","Bácum","Banámichi","Baviácora","Bavispe",
               "Benjamín Hill","Caborca","Cajeme","Cananea","Carbó","La Colorada","Cucurpe","Cumpas","Divisaderos","Empalme","Etchojoa","Fronteras","Granados","Guaymas",
               "Hermosillo","Huachinera","Huásabas","Huatabampo","Huépac","Imuris","Magdalena","Mazatán","Moctezuma","Naco","Nácori Chico","Nacozari de García","Navojoa",
               "Nogales","Onavas","Opodepe","Oquitoa","Pitiquito","Puerto Peñasco","Quiriego","Rayón","Rosario","Sahuaripa","San Felipe de Jesús","San Javier",
               "San Luis Río Colorado","San Miguel de Horcasitas","San Pedro de la Cueva","Santa Ana","Santa Cruz","Sáric","Soyopa","Suaqui Grande","Tepache","Trincheras",
               "Tubutama","Ures","Villa Hidalgo","Villa Pesqueira","General Plutarco Elías Calles","Benito Juárez","San Ignacio Río Muerto",],
    'Tabasco': [
                "Balancán","Cárdenas","Centla","Centro","Comalcalco","Cunduacán","Emiliano Zapata","Huimanguillo","Jalapa","Jalpa de Méndez","Jonuta","Macuspana","Nacajuca",
                "Paraíso","Tacotalpa","Teapa","Tenosique",],
    'Tamaulipas': [
                   "Abasolo","Aldama","Altamira","Antiguo Morelos","Burgos","Bustamante","Camargo","Casas","Ciudad Madero","Cruillas","Gómez Farías","González","Güémez",
                   "Guerrero","Gustavo Díaz Ordaz","Hidalgo","Jaumave","Jiménez","Llera","Mainero","El Mante","Matamoros","Méndez","Mier","Miguel Alemán","Miquihuana",
                   "Nuevo Laredo","Nuevo Morelos","Ocampo","Padilla","Palmillas","Reynosa","Río Bravo","San Carlos","San Fernando","San Nicolás","Soto la Marina","Tampico",
                   "Tula","Valle Hermoso","Victoria","Villagrán","Xicoténcatl"],
    'Tlaxcala': [
                 "Amaxac de Guerrero","Apetatitlán de Antonio Carvajal","Atlangatepec","Atltzayanca","Apizaco","Calpulalpan","El Carmen Tequexquitla","Cuapiaxtla",
                 "Cuaxomulco","Chiautempan","Muñoz de Domingo Arenas","Españita","Huamantla","Hueyotlipan","Ixtacuixtla de Mariano Matamoros","Ixtenco",
                 "Mazatecochco de José María Morelos","Contla de Juan Cuamatzi","Tepetitla de Lardizábal","Sanctórum de Lázaro Cárdenas","Nanacamilpa de Mariano Arista",
                 "Acuamanala de Miguel Hidalgo","Natívitas","Panotla","San Pablo del Monte","Santa Cruz Tlaxcala","Tenancingo","Teolocholco","Tepeyanco","Terrenate",
                 "Tetla de la Solidaridad","Tetlatlahuca","Tlaxcala","Tlaxco","Tocatlán","Totolac","Ziltlaltépec de Trinidad Sánchez Santos","Tzompantepec","Xaloztoc",
                 "Xaltocan","Papalotla de Xicohténcatl","Xicohtzinco","Yauhquemehcan","Zacatelco","Benito Juárez","Emiliano Zapata","Lázaro Cárdenas",
                 "La Magdalena Tlaltelulco","San Damián Texóloc","San Francisco Tetlanohcan","San Jerónimo Zacualpan","San José Teacalco","San Juan Huactzinco",
                 "San Lorenzo Axocomanitla","San Lucas Tecopilco","Santa Ana Nopalucan","Santa Apolonia Teacalco","Santa Catarina Ayometla","Santa Cruz Quilehtla",
                 "Santa Isabel Xiloxoxtla"],
    'Veracruz de Ignacio de la Llave': [
                                        "Acajete","Acatlán","Acayucan","Actopan","Acula","Acultzingo","Camarón de Tejeda","Alpatláhuac","Alto Lucero de Gutiérrez Barrios",
                                        "Altotonga","Alvarado","Amatitlán","Naranjos Amatlán","Amatlán de los Reyes","Angel R. Cabada","La Antigua","Apazapan","Aquila",
                                        "Astacinga","Atlahuilco","Atoyac","Atzacan","Atzalan","Tlaltetela","Ayahualulco","Banderilla","Benito Juárez","Boca del Río",
                                        "Calcahualco","Camerino Z. Mendoza","Carrillo Puerto","Catemaco","Cazones de Herrera","Cerro Azul","Citlaltépetl","Coacoatzintla",
                                        "Coahuitlán","Coatepec","Coatzacoalcos","Coatzintla","Coetzala","Colipa","Comapa","Córdoba","Cosamaloapan de Carpio",
                                        "Cosautlán de Carvajal","Coscomatepec","Cosoleacaque","Cotaxtla","Coxquihui","Coyutla","Cuichapa","Cuitláhuac","Chacaltianguis",
                                        "Chalma","Chiconamel","Chiconquiaco","Chicontepec","Chinameca","Chinampa de Gorostiza","Las Choapas","Chocamán","Chontla","Chumatlán",
                                        "Emiliano Zapata","Espinal","Filomeno Mata","Fortín","Gutiérrez Zamora","Hidalgotitlán","Huatusco","Huayacocotla","Hueyapan de Ocampo",
                                        "Huiloapan de Cuauhtémoc","Ignacio de la Llave","Ilamatlán","Isla","Ixcatepec","Ixhuacán de los Reyes","Ixhuatlán del Café",
                                        "Ixhuatlancillo","Ixhuatlán del Sureste","Ixhuatlán de Madero","Ixmatlahuacan","Ixtaczoquitlán","Jalacingo","Xalapa","Jalcomulco",
                                        "Jáltipan","Jamapa","Jesús Carranza","Xico","Jilotepec","Juan Rodríguez Clara","Juchique de Ferrer","Landero y Coss","Lerdo de Tejada",
                                        "Magdalena","Maltrata","Manlio Fabio Altamirano","Mariano Escobedo","Martínez de la Torre","Mecatlán","Mecayapan","Medellín de Bravo",
                                        "Miahuatlán","Las Minas","Minatitlán","Misantla","Mixtla de Altamirano","Moloacán","Naolinco","Naranjal","Nautla","Nogales","Oluta",
                                        "Omealca","Orizaba","Otatitlán","Oteapan","Ozuluama de Mascareñas","Pajapan","Pánuco","Papantla","Paso del Macho","Paso de Ovejas",
                                        "La Perla","Perote","Platón Sánchez","Playa Vicente","Poza Rica de Hidalgo","Las Vigas de Ramírez","Pueblo Viejo","Puente Nacional",
                                        "Rafael Delgado","Rafael Lucio","Los Reyes","Río Blanco","Saltabarranca","San Andrés Tenejapan","San Andrés Tuxtla",
                                        "San Juan Evangelista","Santiago Tuxtla","Sayula de Alemán","Soconusco","Sochiapa","Soledad Atzompa","Soledad de Doblado","Soteapan",
                                        "Tamalín","Tamiahua","Tampico Alto","Tancoco","Tantima","Tantoyuca","Tatatila","Castillo de Teayo","Tecolutla","Tehuipango",
                                        "Álamo Temapache","Tempoal","Tenampa","Tenochtitlán","Teocelo","Tepatlaxco","Tepetlán","Tepetzintla","Tequila","José Azueta","Texcatepec",
                                        "Texhuacán","Texistepec","Tezonapa","Tierra Blanca","Tihuatlán","Tlacojalpan","Tlacolulan","Tlacotalpan","Tlacotepec de Mejía",
                                        "Tlachichilco","Tlalixcoyan","Tlalnelhuayocan","Tlapacoyan","Tlaquilpa","Tlilapan","Tomatlán","Tonayán","Totutla","Tuxpan","Tuxtilla",
                                        "Ursulo Galván","Vega de Alatorre","Veracruz","Villa Aldama","Xoxocotla","Yanga","Yecuatla","Zacualpan","Zaragoza","Zentla","Zongolica",
                                        "Zontecomatlán de López y Fuentes","Zozocolco de Hidalgo","Agua Dulce","El Higo","Nanchital de Lázaro Cárdenas del Río","Tres Valles",
                                        "Carlos A. Carrillo","Tatahuicapan de Juárez","Uxpanapa","San Rafael","Santiago Sochiapan"],
    'Yucatán': [
                "Abalá","Acanceh","Akil","Baca","Bokobá","Buctzotz","Cacalchén","Calotmul","Cansahcab","Cantamayec","Celestún","Cenotillo","Conkal","Cuncunul","Cuzamá",
                "Chacsinkín","Chankom","Chapab","Chemax","Chicxulub Pueblo","Chichimilá","Chikindzonot","Chocholá","Chumayel","Dzán","Dzemul","Dzidzantún",
                "Dzilam de Bravo","Dzilam González","Dzitás","Dzoncauich","Espita","Halachó","Hocabá","Hoctún","Homún","Huhí","Hunucmá","Ixil","Izamal","Kanasín","Kantunil",
                "Kaua","Kinchil","Kopomá","Mama","Maní","Maxcanú","Mayapán","Mérida","Mocochá","Motul","Muna","Muxupip","Opichén","Oxkutzcab","Panabá","Peto","Progreso",
                "Quintana Roo","Río Lagartos","Sacalum","Samahil","Sanahcat","San Felipe","Santa Elena","Seyé","Sinanché","Sotuta","Sucilá","Sudzal","Suma","Tahdziú",
                "Tahmek","Teabo","Tecoh","Tekal de Venegas","Tekantó","Tekax","Tekit","Tekom","Telchac Pueblo","Telchac Puerto","Temax","Temozón","Tepakán","Tetiz","Teya",
                "Ticul","Timucuy","Tinum","Tixcacalcupul","Tixkokob","Tixmehuac","Tixpéhual","Tizimín","Tunkás","Tzucacab","Uayma","Ucú","Umán","Valladolid","Xocchel",
                "Yaxcabá","Yaxkukul","Yobaín",],
    'Zacatecas': [
                  "Apozol","Apulco","Atolinga","Benito Juárez","Calera","Cañitas de Felipe Pescador","Concepción del Oro","Cuauhtémoc","Chalchihuites","Fresnillo",
                  "Trinidad García de la Cadena","Genaro Codina","General Enrique Estrada","General Francisco R. Murguía","El Plateado de Joaquín Amaro",
                  "General Pánfilo Natera","Guadalupe","Huanusco","Jalpa","Jerez","Jiménez del Teul","Juan Aldama","Juchipila","Loreto","Luis Moya","Mazapil",
                  "Melchor Ocampo","Mezquital del Oro","Miguel Auza","Momax","Monte Escobedo",,"Morelos","Moyahua de Estrada","Nochistlán de Mejía","Noria de Ángeles",
                  "Ojocaliente","Pánuco","Pinos","Río Grande","Sain Alto","El Salvador","Sombrerete","Susticacán","Tabasco","Tepechitlán","Tepetongo",
                  "Teúl de González Ortega","Tlaltenango de Sánchez Román","Valparaíso","Vetagrande","Villa de Cos","Villa García","Villa González Ortega","Villa Hidalgo",
                  "Villanueva","Zacatecas","Trancoso","Santa María de la Paz"]
}


class UiForm(QWidget):
    def __init__(self):
        super().__init__()
        self.setup_ui(self)

    def setup_ui(self, form):
        form.setObjectName("form")
        form.resize(1024, 680)
        form.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.fra_encabezado = QtWidgets.QFrame(form)
        self.fra_encabezado.setGeometry(QtCore.QRect(0, 0, 1024, 60))
        self.fra_encabezado.setMinimumSize(QtCore.QSize(949, 0))
        self.fra_encabezado.setMaximumSize(QtCore.QSize(1024, 16777215))
        self.fra_encabezado.setStyleSheet("QFrame{background-color: #14a085;}")
        self.fra_encabezado.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.fra_encabezado.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fra_encabezado.setObjectName("fra_encabezado")
        self.lbl_titulo = QtWidgets.QLabel(self.fra_encabezado)
        self.lbl_titulo.setGeometry(QtCore.QRect(222, 6, 580, 50))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_titulo.sizePolicy().hasHeightForWidth())
        self.lbl_titulo.setSizePolicy(sizePolicy)
        self.lbl_titulo.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_titulo.setObjectName("lbl_titulo")
        self.verticalLayoutWidget = QtWidgets.QWidget(form)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 160, 981, 421))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.verticalLayoutWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.lbl_municipio = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_municipio.sizePolicy().hasHeightForWidth())
        self.lbl_municipio.setSizePolicy(sizePolicy)
        self.lbl_municipio.setMaximumSize(QtCore.QSize(400, 43))
        self.lbl_municipio.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_municipio.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_municipio.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_municipio.setObjectName("lbl_municipio")
        self.gridLayout.addWidget(self.lbl_municipio, 0, 2, 1, 1)
        self.lbl_cp = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_cp.sizePolicy().hasHeightForWidth())
        self.lbl_cp.setSizePolicy(sizePolicy)
        self.lbl_cp.setMaximumSize(QtCore.QSize(400, 43))
        self.lbl_cp.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_cp.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_cp.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_cp.setObjectName("lbl_cp")
        self.gridLayout.addWidget(self.lbl_cp, 6, 2, 1, 1)
        self.txt_num = QtWidgets.QPlainTextEdit(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_num.sizePolicy().hasHeightForWidth())
        self.txt_num.setSizePolicy(sizePolicy)
        self.txt_num.setMinimumSize(QtCore.QSize(250, 45))
        self.txt_num.setMaximumSize(QtCore.QSize(250, 45))
        self.txt_num.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_num.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_num.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_num.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_num.setTabChangesFocus(True)
        self.txt_num.setObjectName("txt_num")
        self.gridLayout.addWidget(self.txt_num, 3, 1, 1, 1)
        self.txt_calle = QtWidgets.QPlainTextEdit(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_calle.sizePolicy().hasHeightForWidth())
        self.txt_calle.setSizePolicy(sizePolicy)
        self.txt_calle.setMinimumSize(QtCore.QSize(250, 45))
        self.txt_calle.setMaximumSize(QtCore.QSize(250, 45))
        self.txt_calle.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_calle.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_calle.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_calle.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_calle.setTabChangesFocus(True)
        self.txt_calle.setObjectName("txt_calle")
        self.gridLayout.addWidget(self.txt_calle, 1, 1, 1, 1)
        self.lbl_pass = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.lbl_pass.setMaximumSize(QtCore.QSize(16777215, 43))
        self.lbl_pass.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_pass.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_pass.setObjectName("lbl_pass")
        self.gridLayout.addWidget(self.lbl_pass, 2, 0, 1, 1)
        self.txt_colonia = QtWidgets.QPlainTextEdit(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_colonia.sizePolicy().hasHeightForWidth())
        self.txt_colonia.setSizePolicy(sizePolicy)
        self.txt_colonia.setMinimumSize(QtCore.QSize(250, 45))
        self.txt_colonia.setMaximumSize(QtCore.QSize(250, 45))
        self.txt_colonia.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_colonia.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_colonia.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_colonia.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_colonia.setTabChangesFocus(True)
        self.txt_colonia.setObjectName("txt_colonia")
        self.gridLayout.addWidget(self.txt_colonia, 5, 1, 1, 1)
        self.txt_pass = QtWidgets.QPlainTextEdit(self.verticalLayoutWidget)
        self.txt_pass.setMinimumSize(QtCore.QSize(250, 45))
        self.txt_pass.setMaximumSize(QtCore.QSize(250, 45))
        self.txt_pass.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_pass.setInputMethodHints(QtCore.Qt.ImhHiddenText)
        self.txt_pass.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_pass.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_pass.setTabChangesFocus(True)
        self.txt_pass.setOverwriteMode(True)
        self.txt_pass.setObjectName("txt_pass")
        self.gridLayout.addWidget(self.txt_pass, 3, 0, 1, 1)
        self.txt_cp = QtWidgets.QPlainTextEdit(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_cp.sizePolicy().hasHeightForWidth())
        self.txt_cp.setSizePolicy(sizePolicy)
        self.txt_cp.setMinimumSize(QtCore.QSize(250, 45))
        self.txt_cp.setMaximumSize(QtCore.QSize(250, 45))
        self.txt_cp.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_cp.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_cp.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_cp.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_cp.setTabChangesFocus(True)
        self.txt_cp.setTabStopWidth(6)
        self.txt_cp.setObjectName("txt_cp")
        self.gridLayout.addWidget(self.txt_cp, 7, 2, 1, 1)
        self.txt_localidad = QtWidgets.QPlainTextEdit(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_localidad.sizePolicy().hasHeightForWidth())
        self.txt_localidad.setSizePolicy(sizePolicy)
        self.txt_localidad.setMinimumSize(QtCore.QSize(250, 45))
        self.txt_localidad.setMaximumSize(QtCore.QSize(250, 45))
        self.txt_localidad.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_localidad.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_localidad.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_localidad.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_localidad.setTabChangesFocus(True)
        self.txt_localidad.setObjectName("txt_localidad")
        self.gridLayout.addWidget(self.txt_localidad, 7, 1, 1, 1)
        self.lbl_colonia = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_colonia.sizePolicy().hasHeightForWidth())
        self.lbl_colonia.setSizePolicy(sizePolicy)
        self.lbl_colonia.setMaximumSize(QtCore.QSize(400, 43))
        self.lbl_colonia.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_colonia.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_colonia.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_colonia.setObjectName("lbl_colonia")
        self.gridLayout.addWidget(self.lbl_colonia, 4, 1, 1, 1)
        self.lbl_usuario = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_usuario.sizePolicy().hasHeightForWidth())
        self.lbl_usuario.setSizePolicy(sizePolicy)
        self.lbl_usuario.setMaximumSize(QtCore.QSize(16777215, 43))
        self.lbl_usuario.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_usuario.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_usuario.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_usuario.setObjectName("lbl_usuario")
        self.gridLayout.addWidget(self.lbl_usuario, 0, 0, 1, 1)
        self.lbl_estado = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_estado.sizePolicy().hasHeightForWidth())
        self.lbl_estado.setSizePolicy(sizePolicy)
        self.lbl_estado.setMaximumSize(QtCore.QSize(400, 43))
        self.lbl_estado.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_estado.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_estado.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_estado.setObjectName("lbl_estado")
        self.gridLayout.addWidget(self.lbl_estado, 2, 2, 1, 1)
        self.txt_usuario = QtWidgets.QPlainTextEdit(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_usuario.sizePolicy().hasHeightForWidth())
        self.txt_usuario.setSizePolicy(sizePolicy)
        self.txt_usuario.setMinimumSize(QtCore.QSize(250, 45))
        self.txt_usuario.setMaximumSize(QtCore.QSize(250, 45))
        self.txt_usuario.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_usuario.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_usuario.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_usuario.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_usuario.setTabChangesFocus(True)
        self.txt_usuario.setObjectName("txt_usuario")
        self.gridLayout.addWidget(self.txt_usuario, 1, 0, 1, 1)
        self.lbl_calle = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_calle.sizePolicy().hasHeightForWidth())
        self.lbl_calle.setSizePolicy(sizePolicy)
        self.lbl_calle.setMaximumSize(QtCore.QSize(400, 43))
        self.lbl_calle.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_calle.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_calle.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_calle.setObjectName("lbl_calle")
        self.gridLayout.addWidget(self.lbl_calle, 0, 1, 1, 1)
        self.txt_rfc = QtWidgets.QPlainTextEdit(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_rfc.sizePolicy().hasHeightForWidth())
        self.txt_rfc.setSizePolicy(sizePolicy)
        self.txt_rfc.setMinimumSize(QtCore.QSize(250, 45))
        self.txt_rfc.setMaximumSize(QtCore.QSize(250, 45))
        self.txt_rfc.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_rfc.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_rfc.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_rfc.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_rfc.setTabChangesFocus(True)
        self.txt_rfc.setObjectName("txt_rfc")
        self.gridLayout.addWidget(self.txt_rfc, 5, 0, 1, 1)
        self.lbl_rfc = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_rfc.sizePolicy().hasHeightForWidth())
        self.lbl_rfc.setSizePolicy(sizePolicy)
        self.lbl_rfc.setMaximumSize(QtCore.QSize(16777215, 43))
        self.lbl_rfc.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_rfc.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_rfc.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_rfc.setObjectName("lbl_rfc")
        self.gridLayout.addWidget(self.lbl_rfc, 4, 0, 1, 1)
        self.bx_estado = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.bx_estado.setMinimumSize(QtCore.QSize(250, 45))
        self.bx_estado.setMaximumSize(QtCore.QSize(250, 45))
        self.bx_estado.setObjectName("bx_estado")
        self.gridLayout.addWidget(self.bx_estado, 3, 2, 1, 1)
        self.lbl_num = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_num.sizePolicy().hasHeightForWidth())
        self.lbl_num.setSizePolicy(sizePolicy)
        self.lbl_num.setMaximumSize(QtCore.QSize(400, 43))
        self.lbl_num.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_num.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_num.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_num.setObjectName("lbl_num")
        self.gridLayout.addWidget(self.lbl_num, 2, 1, 1, 1)
        self.lbl_localidad = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_localidad.sizePolicy().hasHeightForWidth())
        self.lbl_localidad.setSizePolicy(sizePolicy)
        self.lbl_localidad.setMaximumSize(QtCore.QSize(400, 43))
        self.lbl_localidad.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_localidad.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_localidad.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_localidad.setObjectName("lbl_localidad")
        self.gridLayout.addWidget(self.lbl_localidad, 6, 1, 1, 1)
        self.lbl_email = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_email.sizePolicy().hasHeightForWidth())
        self.lbl_email.setSizePolicy(sizePolicy)
        self.lbl_email.setMaximumSize(QtCore.QSize(16777215, 43))
        self.lbl_email.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_email.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_email.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_email.setObjectName("lbl_email")
        self.gridLayout.addWidget(self.lbl_email, 6, 0, 1, 1)
        self.txt_email = QtWidgets.QPlainTextEdit(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_email.sizePolicy().hasHeightForWidth())
        self.txt_email.setSizePolicy(sizePolicy)
        self.txt_email.setMinimumSize(QtCore.QSize(250, 45))
        self.txt_email.setMaximumSize(QtCore.QSize(250, 45))
        self.txt_email.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_email.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_email.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_email.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_email.setTabChangesFocus(True)
        self.txt_email.setObjectName("txt_email")
        self.gridLayout.addWidget(self.txt_email, 7, 0, 1, 1)
        self.lbl_pais = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_pais.sizePolicy().hasHeightForWidth())
        self.lbl_pais.setSizePolicy(sizePolicy)
        self.lbl_pais.setMaximumSize(QtCore.QSize(400, 43))
        self.lbl_pais.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_pais.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_pais.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_pais.setObjectName("lbl_pais")
        self.gridLayout.addWidget(self.lbl_pais, 4, 2, 1, 1)
        self.bx_pais = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.bx_pais.setMinimumSize(QtCore.QSize(250, 45))
        self.bx_pais.setMaximumSize(QtCore.QSize(250, 45))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(16)
        self.bx_pais.setFont(font)
        self.bx_pais.setStyleSheet("selection-background-color: rgb(37, 174, 136);\n"
                                   "selection-color: rgb(255, 255, 255);")
        self.bx_pais.setObjectName("bx_pais")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("src/banderas/mexico.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon, "")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("src/banderas/gringos.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon1, "")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("src/banderas/canada.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon2, "")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("src/banderas/guatemala.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon3, "")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("src/banderas/belice.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon4, "")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap("src/banderas/honduras.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon5, "")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap("src/banderas/nicaragua.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon6, "")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap("src/banderas/costa_rica.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon7, "")
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap("src/banderas/salvador.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon8, "")
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap("src/banderas/panama.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon9, "")
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap("src/banderas/argentina.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon10, "")
        icon11 = QtGui.QIcon()
        icon11.addPixmap(QtGui.QPixmap("src/banderas/brasil.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon11, "")
        icon12 = QtGui.QIcon()
        icon12.addPixmap(QtGui.QPixmap("src/banderas/chile.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon12, "")
        icon13 = QtGui.QIcon()
        icon13.addPixmap(QtGui.QPixmap("src/banderas/colombia.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon13, "")
        icon14 = QtGui.QIcon()
        icon14.addPixmap(QtGui.QPixmap("src/banderas/ecuador.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon14, "")
        icon15 = QtGui.QIcon()
        icon15.addPixmap(QtGui.QPixmap("src/banderas/guyana.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon15, "")
        icon16 = QtGui.QIcon()
        icon16.addPixmap(QtGui.QPixmap("src/banderas/paraguay.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon16, "")
        icon17 = QtGui.QIcon()
        icon17.addPixmap(QtGui.QPixmap("src/banderas/peru.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon17, "")
        icon18 = QtGui.QIcon()
        icon18.addPixmap(QtGui.QPixmap("src/banderas/surinam.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon18, "")
        icon19 = QtGui.QIcon()
        icon19.addPixmap(QtGui.QPixmap("src/banderas/trinidad_tobago.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon19, "")
        icon20 = QtGui.QIcon()
        icon20.addPixmap(QtGui.QPixmap("src/banderas/uruguay.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon20, "")
        icon21 = QtGui.QIcon()
        icon21.addPixmap(QtGui.QPixmap("src/banderas/venezuela.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon21, "")
        icon22 = QtGui.QIcon()
        icon22.addPixmap(QtGui.QPixmap("src/banderas/aruba.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon22, "")
        icon23 = QtGui.QIcon()
        icon23.addPixmap(QtGui.QPixmap("src/banderas/bonaire.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon23, "")
        icon24 = QtGui.QIcon()
        icon24.addPixmap(QtGui.QPixmap("src/banderas/curaçao.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon24, "")
        icon25 = QtGui.QIcon()
        icon25.addPixmap(QtGui.QPixmap("src/banderas/guyana_francesa.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon25, "")
        icon26 = QtGui.QIcon()
        icon26.addPixmap(QtGui.QPixmap("src/banderas/islas_georgias.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon26, "")
        icon27 = QtGui.QIcon()
        icon27.addPixmap(QtGui.QPixmap("src/banderas/antigua_barbuda.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon27, "")
        icon28 = QtGui.QIcon()
        icon28.addPixmap(QtGui.QPixmap("src/banderas/bahamas.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon28, "")
        icon29 = QtGui.QIcon()
        icon29.addPixmap(QtGui.QPixmap("src/banderas/barbados.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon29, "")
        icon30 = QtGui.QIcon()
        icon30.addPixmap(QtGui.QPixmap("src/banderas/cuba.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon30, "")
        icon31 = QtGui.QIcon()
        icon31.addPixmap(QtGui.QPixmap("src/banderas/dominica.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon31, "")
        icon32 = QtGui.QIcon()
        icon32.addPixmap(QtGui.QPixmap("src/banderas/granada.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon32, "")
        icon33 = QtGui.QIcon()
        icon33.addPixmap(QtGui.QPixmap("src/banderas/haiti.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon33, "")
        icon34 = QtGui.QIcon()
        icon34.addPixmap(QtGui.QPixmap("src/banderas/jamaica.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon34, "")
        icon35 = QtGui.QIcon()
        icon35.addPixmap(QtGui.QPixmap("src/banderas/puerto_rico.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon35, "")
        icon36 = QtGui.QIcon()
        icon36.addPixmap(QtGui.QPixmap("src/banderas/republica_dominicana.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon36, "")
        icon37 = QtGui.QIcon()
        icon37.addPixmap(QtGui.QPixmap("src/banderas/san_cristobal_nieves.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon37, "")
        icon38 = QtGui.QIcon()
        icon38.addPixmap(QtGui.QPixmap("src/banderas/san_vicente_granadinas.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon38, "")
        icon39 = QtGui.QIcon()
        icon39.addPixmap(QtGui.QPixmap("src/banderas/santa_lucia.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.bx_pais.addItem(icon39, "")
        self.bx_pais.addItem(icon18, "")
        self.gridLayout.addWidget(self.bx_pais, 5, 2, 1, 1)
        self.bx_municipio = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.bx_municipio.setMinimumSize(QtCore.QSize(250, 45))
        self.bx_municipio.setMaximumSize(QtCore.QSize(250, 45))
        self.bx_municipio.setObjectName("bx_municipio")
        self.gridLayout.addWidget(self.bx_municipio, 1, 2, 1, 1)
        self.btn_siguiente = QtWidgets.QPushButton(form)
        self.btn_siguiente.setGeometry(QtCore.QRect(487, 610, 50, 50))
        self.btn_siguiente.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_siguiente.setText("")
        icon40 = QtGui.QIcon()
        icon40.addPixmap(QtGui.QPixmap("src/success.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_siguiente.setIcon(icon40)
        self.btn_siguiente.setIconSize(QtCore.QSize(45, 45))
        self.btn_siguiente.setFlat(True)
        self.btn_siguiente.setObjectName("btn_siguiente")
        self.lbl_nombre = QtWidgets.QLabel(form)
        self.lbl_nombre.setGeometry(QtCore.QRect(70, 80, 250, 27))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_nombre.sizePolicy().hasHeightForWidth())
        self.lbl_nombre.setSizePolicy(sizePolicy)
        self.lbl_nombre.setMaximumSize(QtCore.QSize(16777215, 27))
        self.lbl_nombre.setBaseSize(QtCore.QSize(0, 0))
        self.lbl_nombre.setStyleSheet("font: 12pt \"Serif\";")
        self.lbl_nombre.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.lbl_nombre.setObjectName("lbl_nombre")
        self.txt_nombre = QtWidgets.QPlainTextEdit(form)
        self.txt_nombre.setGeometry(QtCore.QRect(70, 110, 450, 45))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.txt_nombre.sizePolicy().hasHeightForWidth())
        self.txt_nombre.setSizePolicy(sizePolicy)
        self.txt_nombre.setMinimumSize(QtCore.QSize(450, 45))
        self.txt_nombre.setMaximumSize(QtCore.QSize(450, 45))
        self.txt_nombre.setStyleSheet("font: 25 16pt \"Roboto\";")
        self.txt_nombre.setInputMethodHints(QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferLowercase)
        self.txt_nombre.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_nombre.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.txt_nombre.setTabChangesFocus(True)
        self.txt_nombre.setTabStopWidth(60)
        self.txt_nombre.setObjectName("txt_nombre")

        self.retranslateUi(form)
        QtCore.QMetaObject.connectSlotsByName(form)
        form.setTabOrder(self.txt_nombre, self.txt_usuario)
        form.setTabOrder(self.txt_usuario, self.txt_pass)
        form.setTabOrder(self.txt_pass, self.txt_rfc)
        form.setTabOrder(self.txt_rfc, self.txt_email)
        form.setTabOrder(self.txt_email, self.txt_calle)
        form.setTabOrder(self.txt_calle, self.txt_num)
        form.setTabOrder(self.txt_num, self.txt_colonia)
        form.setTabOrder(self.txt_colonia, self.txt_localidad)
        form.setTabOrder(self.txt_localidad, self.bx_municipio)
        form.setTabOrder(self.bx_municipio, self.bx_estado)
        form.setTabOrder(self.bx_estado, self.bx_pais)
        form.setTabOrder(self.bx_pais, self.txt_cp)

    def retranslateUi(self, form):
        _translate = QtCore.QCoreApplication.translate
        form.setWindowTitle(_translate("form", "form"))
        self.lbl_titulo.setText(_translate("form", "Registro"))
        self.lbl_municipio.setText(_translate("form", "Municipio"))
        self.lbl_cp.setText(_translate("form", "CP"))
        self.lbl_pass.setText(_translate("form", "Contraseña"))
        self.txt_pass.setPlaceholderText(_translate("form", "●●●●"))
        self.lbl_colonia.setText(_translate("form", "Colonia"))
        self.lbl_usuario.setText(_translate("form", "Usuario"))
        self.lbl_estado.setText(_translate("form", "Estado"))
        self.lbl_calle.setText(_translate("form", "Calle"))
        self.lbl_rfc.setText(_translate("form", "RFC"))
        self.lbl_num.setText(_translate("form", "No. Exterior"))
        self.lbl_localidad.setText(_translate("form", "Localidad"))
        self.lbl_email.setText(_translate("form", "Correo electrónico"))
        self.lbl_pais.setText(_translate("form", "País"))
        self.bx_pais.setItemText(0, _translate("form", "México"))
        self.bx_pais.setItemText(1, _translate("form", "Estados Unidos"))
        self.bx_pais.setItemText(2, _translate("form", "Canadá"))
        self.bx_pais.setItemText(3, _translate("form", "Guatemala"))
        self.bx_pais.setItemText(4, _translate("form", "Belice"))
        self.bx_pais.setItemText(5, _translate("form", "Honduras"))
        self.bx_pais.setItemText(6, _translate("form", "Nicaragua"))
        self.bx_pais.setItemText(7, _translate("form", "Costa Rica"))
        self.bx_pais.setItemText(8, _translate("form", "El Salvador"))
        self.bx_pais.setItemText(9, _translate("form", "Panamá"))
        self.bx_pais.setItemText(10, _translate("form", "Argentina"))
        self.bx_pais.setItemText(11, _translate("form", "Brasil"))
        self.bx_pais.setItemText(12, _translate("form", "Chile"))
        self.bx_pais.setItemText(13, _translate("form", "Colombia"))
        self.bx_pais.setItemText(14, _translate("form", "Ecuador"))
        self.bx_pais.setItemText(15, _translate("form", "Guyana"))
        self.bx_pais.setItemText(16, _translate("form", "Paraguay"))
        self.bx_pais.setItemText(17, _translate("form", "Perú"))
        self.bx_pais.setItemText(18, _translate("form", "Surinam"))
        self.bx_pais.setItemText(19, _translate("form", "Trinidad y Tobago"))
        self.bx_pais.setItemText(20, _translate("form", "Uruguay"))
        self.bx_pais.setItemText(21, _translate("form", "Venezuela"))
        self.bx_pais.setItemText(22, _translate("form", "Aruba"))
        self.bx_pais.setItemText(23, _translate("form", "Bonaire"))
        self.bx_pais.setItemText(24, _translate("form", "Curaçao"))
        self.bx_pais.setItemText(25, _translate("form", "Guyana Francesa"))
        self.bx_pais.setItemText(26, _translate("form", "Islas Georgias"))
        self.bx_pais.setItemText(27, _translate("form", "Antigua y Barbuda"))
        self.bx_pais.setItemText(28, _translate("form", "Bahamas"))
        self.bx_pais.setItemText(29, _translate("form", "Barbados"))
        self.bx_pais.setItemText(30, _translate("form", "Cuba"))
        self.bx_pais.setItemText(31, _translate("form", "Dominica"))
        self.bx_pais.setItemText(32, _translate("form", "Granada"))
        self.bx_pais.setItemText(33, _translate("form", "Haití"))
        self.bx_pais.setItemText(34, _translate("form", "Jamaica"))
        self.bx_pais.setItemText(35, _translate("form", "Puerto Rico"))
        self.bx_pais.setItemText(36, _translate("form", "República Dominicana"))
        self.bx_pais.setItemText(37, _translate("form", "San Cristobal y Nieves"))
        self.bx_pais.setItemText(38, _translate("form", "San Vicente y las Granadinas"))
        self.bx_pais.setItemText(39, _translate("form", "Santa Lucía"))
        self.bx_pais.setItemText(40, _translate("form", "Surinam"))
        self.btn_siguiente.setShortcut(_translate("form", "Return"))
        self.lbl_nombre.setText(_translate("form", "Nombre Completo"))

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    x = UiForm()
    x.show()
    sys.exit(app.exec_())
