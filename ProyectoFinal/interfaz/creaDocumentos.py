from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
import os
from num2words import num2words
import ini
from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
import time
import random


#doc = SimpleDocTemplate("prueba1.pdf")

lista=[[1,'Refresco',54,0,15],[3,'Agua',10,0,14],[5,'Papas',15,0,18]]#(PRODUCTO(CANTIDAD,DESCRIPCION,VALORUNITARIO,IMPORTE,IMPORTETRASLADO))

receptor=['45136441','Alan Abundis Canales','No 2 colonia la chida','1516321']#(RFC,NOMBRE,DIRECCION,NOCUENTA)
emisor=ini.leer_iniEM()

def importe(lis):
    t=len(lis)
    for i in range(0,t):
        importe = lista[i][0]*lista[i][2]
        lista[i][3]=importe

def subtotal(lis):
    total=0
    t=len(lis)
    for i in range(0,t):
        total += lista[i][3]
    return total

def iva(subtotal):
    iva=subtotal*.16
    return iva

def impuesto(lis):
    impuesto=0
    t=len(lis)
    for i in range(0,t):
        impuesto += lista[i][4]
    return impuesto

def total(subtotal,iva,impuesto):
    total=subtotal+iva+impuesto
    return total


    
def obtenerfecha():
    dia=time.strftime("%d")
    fecha=time.strftime("%d/%m/%Y")
    hora = time.strftime("%I:%M")
    return fecha+"    "+hora

def obtnerrand():
    año=time.strftime("%Y")
    rand=random.randrange(100)
    return str(rand)+"   "+año

def obtenerfolio():
    folio=random.randrange(100000,900000,1)
    return folio

def total_letra(total):
    letra=num2words(total,lang='esp')
    return letra

#variables a incluir en creaPDF la variable lista es temporal
importe(lista)
subtotal=subtotal(lista)
iva=iva(subtotal)
impuesto=impuesto(lista)
total=total(subtotal,iva,impuesto)
total_letra=total_letra(total)
#Estas variables son los datos del receptor son temporales

formadepago='peso'
fechayhora=obtenerfecha()
folioyserie=obtnerrand()
metododepago="cheque"
folio=obtenerfolio()

def creaXML(conceptos,emisor,subtotal,imiva,impuesto,total,totalletra,fechayhora,folioyserie,receptor,metododepago,folio):
    formadepago = "efectivo"
    condicionesdepago = "No Aplica"

    xml=Element('xml')
    xml.set('version','1.0')
    xml.set('encoding','utf-8')
    factura=Element('factura')
    factura.set('Folio y Serie',folioyserie)
    factura.set('Fecha y Hora de Elaboracion',fechayhora)
    factura.set('Folio',str(folio))
    tree=ElementTree(xml)
    xml.append(factura)

    #Emisor
    em=Element('emisor')
    factura.append(em)

    correo=Element('correo')
    em.append(correo)
    correo.text=emisor[0]

    nombre=Element('nombre')
    em.append(nombre)
    nombre.text=emisor[1]

    rfc=Element('rfc')
    em.append(rfc)
    rfc.text=emisor[2]

    domicilioFiscal=Element('DomicilioFiscal')
    em.append(domicilioFiscal)
    domicilioFiscal.text=emisor[3]

    #Receptor
    re=Element('receptor')
    factura.append(re)

    rfcR=Element('rfc')
    re.append(rfcR)
    rfcR.text=receptor[0]

    nombreR=Element('nombre')
    re.append(nombreR)
    nombreR.text=receptor[1]

    direccion=Element('direccion')
    re.append(direccion)
    direccion.text=receptor[1]

    #conceptos
    concep=Element('conceptos')
    factura.append(concep)
    
    t=len(conceptos)
    for i in range(0,t):
        subconcep=Element('concepto')
        concep.append(subconcep)
        
        cantidad=Element('cantidad')
        subconcep.append(cantidad)
        cantidad.text=str(conceptos[i][0])

        descripcion=Element('descripcion')
        subconcep.append(descripcion)
        descripcion.text=conceptos[i][1]

        valorU=Element('valor unitario')
        subconcep.append(valorU)
        valorU.text=str(conceptos[i][2])

        importe=Element('importe')
        subconcep.append(importe)
        importe.text=str(conceptos[i][3])       

    #PAGO   
    pago=Element('Pago')
    factura.append(pago)

    subtot=Element('Subtotal')
    pago.append(subtot)
    subtot.text=str(subtotal)

    iva=Element('Importe Iva')
    pago.append(iva)
    iva.text=str(imiva)

    imp=Element('Impuestos de Traslado')
    pago.append(imp)
    imp.text=str(impuesto)

    tot=Element('Total')
    pago.append(tot)
    tot.text=str(total)

    totL=Element('Total en Letra')
    pago.append(totL)
    totL.text=str(totalletra)

    metodo=Element('Metodo de Pago')
    pago.append(metodo)
    metodo.text=metododepago
    
    forma=Element('Forma de Pago')
    pago.append(forma)
    forma.text=formadepago

    condiciones=Element('Condiciiones de Pago')
    pago.append(condiciones)
    condiciones.text=condicionesdepago
    #escritura
   

    tree.write(str(folio)+'.xml')
    
#FUNCION PARA CREAR PDF  (lista,lista,var,var,var,var,var,var,var,lista,var,var,var,var)
def creaPDF(conceptos,emisor,subtotal,imiva,impuesto,total,totalletra,fechayhora,folioyserie,receptor,metododepago,folio):
    aux = canvas.Canvas(str(folio)+".pdf")
    formadepago = "efectivo"
    condicionesdepago = "No Aplica"

    #EMISOR
    aux.setFontSize(9)
    aux.drawString(50,800,emisor[0])
    aux.drawString(50,780,emisor[1])
    aux.setFontSize(10)
    aux.drawString(50,760,"RFC Emisor: ")
    aux.setFontSize(9)
    aux.drawString(50,740,emisor[2])
    aux.setFontSize(10)
    aux.drawString(50,720,"Domicilio Fiscal Emisor")
    aux.setFontSize(9)
    aux.drawString(50,700,emisor[3])


    #Folios
    aux.setFontSize(10)
    aux.drawString(340,780,"Folio Fiscal ")
    aux.setFontSize(9)
    aux.drawString(340,760,str(folio))
    aux.setFontSize(10)
    aux.drawString(340,740,"Lugar Fecha y hora de elabrocion")
    aux.setFontSize(9)
    aux.drawString(340,720,fechayhora)
    aux.setFontSize(10)
    aux.drawString(340,700,"Folio y Serie ")
    aux.setFontSize(9)
    aux.drawString(340,680,folioyserie)
    aux.setFontSize(10)
    aux.drawString(340,660,"Regimen Fiscal")
    aux.setFontSize(9)
    aux.drawString(340,640,emisor[4])


    #Recptor
    aux.drawString(50,680,"RFC Receptor: ")
    aux.drawString(50,660,receptor[0])
    aux.drawString(50,640,receptor[1])
    aux.drawString(50,620,receptor[2])


    #contenido Tabla
    y=520
    t=len(conceptos)
    for i in range(0,t):
        aux.setFontSize(10)
        aux.drawString(70,y,str(conceptos[i][0]))
        aux.drawString(150,y,str(conceptos[i][1]))
        aux.drawString(390,y,"$"+str(conceptos[i][2]))
        aux.drawString(470,y,"$"+str(conceptos[i][3]))
        y=y-20

    #TABLA#################################################
    #-----
    aux.line(50, 580, 530, 580)
    #|
    aux.line(50, 580, 50, 350)
    #_
    aux.line(50, 350, 530, 350)
    #  |
    aux.line(530,580, 530, 350)

    #Columnas
    aux.setLineWidth(.5)
    aux.drawString(60, 558, "CANTIDAD")
    aux.line(130, 580, 130, 350)
    aux.drawString(200, 558, "DESCRIPCION")
    aux.line(370, 580, 370, 350)
    aux.drawString(385, 565, "PRECIO")
    aux.drawString(380, 550, "UNITARIO")
    aux.line(450, 580, 450, 350)
    aux.drawString(460, 558, "IMPORTE")
    aux.line(50, 545, 530, 545)

    #Texto operaciones
    aux.setFontSize(9)
    aux.drawString(50,330,"Moneda:")
    aux.setFontSize(8)
    aux.drawString(180,330,"Peso")
    aux.setFontSize(9)
    aux.drawString(50,310,"Forma de pago:")
    aux.setFontSize(8)
    aux.drawString(180,310,formadepago)
    aux.setFontSize(9)
    aux.drawString(50,290,"Metodo de pago:")
    aux.setFontSize(8)
    aux.drawString(180,290,metododepago)
    aux.setFontSize(9)
    aux.drawString(50,270,"Numero de cuenta de pago:")
    aux.setFontSize(8)
    aux.drawString(180,270,receptor[3])
    aux.setFontSize(9)
    aux.drawString(50,250,"Condiciones de pago")
    aux.setFontSize(8)
    aux.drawString(50,230,condicionesdepago)
    aux.setFontSize(9)
    aux.drawString(50,200,"Total con letra :")
    aux.setFontSize(8)
    aux.drawString(50,180,total_letra.upper()+" PESOS")
    aux.setFontSize(9)
    aux.drawString(370,330,"Subtotal:")
    aux.setFontSize(9)
    aux.drawString(470,330,"$"+str(subtotal))
    aux.setFontSize(9)
    aux.drawString(370,300,"Impuestos Traslado:")
    aux.setFontSize(9)
    aux.drawString(470,300,"$"+str(impuesto))
    aux.setFontSize(9)
    aux.drawString(370,270,"IVA  16%:")
    aux.setFontSize(9)
    aux.drawString(470,270,"$"+str(imiva))
    aux.setFontSize(9)
    aux.drawString(370,240,"Total:")
    aux.setFontSize(9)
    aux.drawString(470,240,"$"+str(total))



    #CFDI
    aux.setFontSize(8)
    aux.drawString(50,160,"Sello digital del CFDI")
    aux.setFontSize(6)
    aux.drawString(50,150,"W9n3iMd370R1ZwOQdV20wfaSXPMTA3FOr5BZwfljM+Ci6U9Kdd6ubbK6oNDZe49fsCiRVUQjQWqpt0PiG23GiQWcjzCE7KTKMYh7I")
    aux.drawString(50,142,"+sGQ9aSYzdMEZRn6kLGGvSPzP94tPdxIVPPDLT99ocs2vW7/UpN1KelszgStNLps+gpUTc=")
    aux.setFontSize(8)
    aux.drawString(50,132,"Sello del SAT")
    aux.setFontSize(6)
    aux.drawString(50,122,"B9UcHJYaT+UX3bJKxCf6zcwZbde22+Ruo8Z7YyoQe3Hyhz40QvXPQc")
    aux.drawString(50,114,"+TkLSQCuIQ0+XGrx/F7RkPX1xAchr5s5xrDfAaqCMdRd2L3mqJ7ZfnF0Fj8ZVHkohdbWM/POfGF2M4/9DiEWnCfhQjIi1jy6WkEU1zLeZelxirNB8w4Jw=")
    aux.setFontSize(8)
    aux.drawString(170,104,"Cadena Original del complemento de certificación digital del SAT")
    aux.setFontSize(6)
    aux.drawString(170,94,"|1.0|6F7353A7-A64F-40C0-AC20-8F43EEE4E1D2|2016-04-18T12:27:06|W9n3iMd370R1ZwOQdV20wfaSXPMTA3FOr5BZwfljM")
    aux.drawString(170,86,"+Ci6U9Kdd6ubbK6oNDZe49fsCiRVUQjQWqpt0PiG23GiQWcjzCE7KTKMYh7I")
    aux.drawString(170,78,"+sGQ9aSYzdMEZRn6kLGGvSPzP94tPdxIVPPDLT99ocs2vW7/UpN1KelszgStNLps+gpUTc=|00001000000201748120||")
    aux.setFontSize(8)
    aux.drawString(170,58,"No de Serie del Certificado del SAT:           00001000000201748120")

    aux.drawImage("D:\\Users\\Alan\\Dropbox\\DAADProyecto\\Interface\\CODIGO QR.png",50,10,100,100)
    aux.showPage()
    aux.save()

    os.popen(str(folio)+'.pdf')

##### PRUEBA DE LAS FUNCIONES
creaPDF(lista,emisor,subtotal,iva,impuesto,total,total_letra,fechayhora,folioyserie,receptor,metododepago,folio)
creaXML(lista,emisor,subtotal,iva,impuesto,total,total_letra,fechayhora,folioyserie,receptor,metododepago,folio)

