#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QMessageBox


class Form(QWidget):
    def __init__(self):
        super().__init__()
        self.setup_ui(self)

    def setup_ui(self, frm):
        print("Iniciando interfaz")
        frm.setObjectName("Prueba")
        frm.setWindowTitle("Prueba")
        frm.setWindowModality(QtCore.Qt.ApplicationModal)
        frm.resize(100, 100)
        frm.setStyleSheet("background-color: #ffffff;")
        frm.move(1800, 100)
        self.boton = QtWidgets.QPushButton(self)
        self.boton.setGeometry(QtCore.QRect(25, 25, 50, 50))
        self.boton.setText("xD")
        self.boton.setStyleSheet("QPushButton:hover {"
                                 "border-width: 1px;"
                                 "border-radius: 5px;"
                                 "background-color: #eeeeee;"
                                 "color: #333333;"
                                 "}")
        self.boton.setFlat(True)
        self.boton.setObjectName("boton")
        self.boton.clicked.connect(self.accion)

    def accion(self):
        mensaje = QMessageBox()
        # print("Presionando botón")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap('../src/logo.png'), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        mensaje.setWindowIcon(icon)
        QMessageBox.about(mensaje, 'xD', 'Has presionado el botón')

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    x = Form()
    x.show()
    sys.exit(app.exec_())
