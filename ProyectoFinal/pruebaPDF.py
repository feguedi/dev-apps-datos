from reportlab.pdfgen import canvas
"""from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle"""
import os

aux = canvas.Canvas("prueba1.pdf")
# doc = SimpleDocTemplate("prueba1.pdf")

# EMISOR
aux.setFontSize(9)
aux.drawString(50, 800, "variable Correo Electornico Emisor")  # Variable e-mail emisor
aux.drawString(50, 780, "Variable Nombre Emisor")  # Variable nombre emisor
aux.setFontSize(10)
aux.drawString(50, 760, "RFC Emisor: ")
aux.setFontSize(9)
aux.drawString(50, 740, "variable RFC emisor")  # Variable RFC emisor
aux.setFontSize(10)
aux.drawString(50, 720, "Domicilio Fiscal Emisor")
aux.setFontSize(9)
aux.drawString(50, 700, "variable domicilio fiscal emisor")  # Variable domicilio fiscal emisor

# Folios
aux.setFontSize(10)
aux.drawString(340, 780, "Folio Fiscal ")
aux.setFontSize(9)
aux.drawString(340, 760, "variable Folio Fiscal ")  # Variable folio fiscal
aux.setFontSize(10)
aux.drawString(340, 740, "Lugar Fecha y hora de elabrocion")
aux.setFontSize(9)
aux.drawString(340, 720, "variable Lugar Fecha y Hora de elaboracion")  # Variable lugar, fecha y hora de elaboración
aux.setFontSize(10)
aux.drawString(340, 700, "Folio y Serie ")
aux.setFontSize(9)
aux.drawString(340, 680, "variable Folio y Serie ")  # Variable folio y srie
aux.setFontSize(10)
aux.drawString(340, 660, "Regimen Fiscal")
aux.setFontSize(9)
aux.drawString(340, 640, "variable Regimen fiscal")  # Variable régimen fiscal

# Recptor
aux.drawString(50, 680, "RFC Receptor: ")
aux.drawString(50, 660, " RFC Receptor: ")  # Variable RFC receptor
aux.drawString(50, 640, " Nombre Receptor")
aux.drawString(50, 620, "variable Direccion Receptor")  # Variable direción receptor

# TABLA#################################################
# -----
aux.line(50, 580, 530, 580)
# |
aux.line(50, 580, 50, 350)
#
aux.line(50, 350, 530, 350)
#  |
aux.line(530, 580, 530, 350)

# Columnas
aux.setLineWidth(.5)
aux.drawString(60, 558, "CANTIDAD")
aux.line(130, 580, 130, 350)
aux.drawString(200, 558, "DESCRIPCION")
aux.line(370, 580, 370, 350)
aux.drawString(385, 565, "PRECIO")
aux.drawString(380, 550, "UNITARIO")
aux.line(450, 580, 450, 350)
aux.drawString(460, 558, "IMPORTE")
aux.line(50, 545, 530, 545)

# Texto operaciones
aux.setFontSize(9)
aux.drawString(50, 330, "Moneda:")  # Moneda
aux.setFontSize(8)
aux.drawString(180, 330, "Peso")  # Peso
aux.setFontSize(9)
aux.drawString(50, 310, "Forma de pago:")
aux.setFontSize(8)
aux.drawString(180, 310, "Var forma de pago:")  # Variable forma de pago
aux.setFontSize(9)
aux.drawString(50, 290, "Metodo de pago:")
aux.setFontSize(8)
aux.drawString(180, 290, "Var metodo de pago:")  # Variable método de pago
aux.setFontSize(9)
aux.drawString(50, 270, "Numero de cuenta de pago:")
aux.setFontSize(8)
aux.drawString(180, 270, "Var No cuenta de pago:")  # Variable número de cuenta de pago
aux.setFontSize(9)
aux.drawString(50, 250, "Condiciones de pago:")
aux.setFontSize(8)
aux.drawString(50, 230, "Var Condiciones de pago")  # Variable condiciones de pago
aux.setFontSize(9)
aux.drawString(50, 200, "TOTAL con letra :")  # Total con letra
aux.setFontSize(8)
aux.drawString(50, 180, "Var total con letra")  # Variable total con letra
aux.setFontSize(9)
aux.drawString(370, 330, "Subtotal:")
aux.setFontSize(8)
aux.drawString(470, 330, "Var Subtotal")  # Variable subtotal
aux.setFontSize(9)
aux.drawString(370, 300, "Impuestos Traslado:")
aux.setFontSize(8)
aux.drawString(470, 300, "Var Impuesto")  # Variable impuesto
aux.setFontSize(9)
aux.drawString(370, 270, "IVA  16%:")
aux.setFontSize(8)
aux.drawString(470, 270, "Var IVA")  # Variable IVA
aux.setFontSize(9)
aux.drawString(370, 240, "TOTAL:")
aux.setFontSize(8)
aux.drawString(470, 240, "Var TOTAL")  # Variable Total

# CFDI
aux.setFontSize(8)
aux.drawString(50, 160, "Sello digital del CFDI")
aux.setFontSize(6)
aux.drawString(50, 150,
               "W9n3iMd370R1ZwOQdV20wfaSXPMTA3FOr5BZwfljM+Ci6U9Kdd6ubbK6oNDZe49fsCiRVUQjQWqpt0PiG23GiQWcjzCE7KTKMYh7I")
aux.drawString(50, 142, "+sGQ9aSYzdMEZRn6kLGGvSPzP94tPdxIVPPDLT99ocs2vW7/UpN1KelszgStNLps+gpUTc=")
aux.setFontSize(8)
aux.drawString(50, 132, "Sello del SAT")
aux.setFontSize(6)
aux.drawString(50, 122, "B9UcHJYaT+UX3bJKxCf6zcwZbde22+Ruo8Z7YyoQe3Hyhz40QvXPQc")
aux.drawString(50, 114,
               "+TkLSQCuIQ0+XGrx/F7RkPX1xAchr5s5xrDfAaqCMdRd2L3mqJ7ZfnF0Fj8ZVHkohdbWM/POfGF2M4/9DiEWnCfhQjIi1jy6WkEU1zL"
               "eZelxirNB8w4Jw=")
aux.setFontSize(8)
aux.drawString(170, 104, "Cadena Original del complemento de certificación digital del SAT")
aux.setFontSize(6)
aux.drawString(170, 94,
               "|1.0|6F7353A7-A64F-40C0-AC20-8F43EEE4E1D2|2016-04-18T12:27:06|W9n3iMd370R1ZwOQdV20wfaSXPMTA3FOr5BZwflj"
               "M")
aux.drawString(170, 86, "+Ci6U9Kdd6ubbK6oNDZe49fsCiRVUQjQWqpt0PiG23GiQWcjzCE7KTKMYh7I")
aux.drawString(170, 78,
               "+sGQ9aSYzdMEZRn6kLGGvSPzP94tPdxIVPPDLT99ocs2vW7/UpN1KelszgStNLps+gpUTc=|00001000000201748120||")
aux.setFontSize(8)
aux.drawString(170, 58, "No de Serie del Certificado del SAT:           00001000000201748120")

ruta_qr = os.path.dirname(os.path.abspath(__file__))  # Dirección del directorio de este archivo
aux.drawImage(os.path.join(ruta_qr, "CODIGO QR.png"), 50, 10, 100, 100)
aux.showPage()
aux.save()

os.popen('prueba1.pdf')
