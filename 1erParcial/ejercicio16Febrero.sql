-- Seleccionar los empleados del departamentos de ventas, su salario
-- y comisión de todos aquellos cuyo salario íntegro (salario + 
-- comisión) sea superior a 3,000,000

SELECT EMP_NOMBRE, EMP_SALARIO, EMP_COMISION, EMP_SALARIO + IFNULL(EMP_COMISION,0)
FROM EMPLEADOS
WHERE DEP_ID = 30 AND (EMP_SALARIO + IFNULL(EMP_COMISION,0)) > 3000000;

-- Seleccionar los empleados y el nombre del departamento al que 
-- pertenecen de todos aquellos empleados que tengan una antigüedad
-- mayor a tres años, tengan asignado un jefe, sin percibir ninguna
-- comisión.

SELECT emp.EMP_NOMBRE, emp.DEP_ID, dep.DEP_ID, dep.DEP_NOMBRE
FROM EMPLEADOS emp, DEPARTAMENTOS dep
WHERE emp.EMP_FECHACONTRATO >= '12-10-08' AND IFNULL(emp.EMP_JEFE,0) AND emp.EMP_COMISION = NULL;

-- Elaborar una lista de los cargos que actualmente están ocupados
-- en la empresa por lo menos un empleado y ordenarlos por su
-- nombre de manera descendente

SELECT EMP_NOMBRE, EMP_CARGO
FROM EMPLEADOS
WHERE IN('PRESIDENTE', 'GERENTE', 'VENDEDOR', 'OFICINISTA', 'ANALISTA') AND EMP_NOMBRE DESC;

-- Seleccionar el nombre del empleado, su salario, comisión y
-- salario íntegro (salario + comisión) de aquellos empleados
-- contratados en el año 2012, pertenecientes a cualquier
-- departamento excepto al 20, sin considerar aquellos cuyo nombre
-- tenga incluida una letra "a". Agregar una última columna con una
-- literal "Este empleado será despedido en el próximo mes
-- '01-03-2015'"

SELECT EMP_NOMBRE, EMP_SALARIO, EMP_COMISION, EMP_SALARIO + EMP_COMISION "Este empleado será despedido el próximo mes '01-03-2015'"
FROM EMPLEADOS
WHERE EMP_FECHACONTRATO > '12-01-01' AND DEP_ID <> '20' NOT (DEP_ID = '20' AND EMP_NOMBRE = '%A');