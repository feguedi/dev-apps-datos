CREATE TABLE EMPLEADOS (
	emp_id				INTEGER(4),
	emp_nombre			VARCHAR(40),
	emp_cargo			VARCHAR(10),
	emp_jefe			INTEGER(4),
	emp_fechacontrato	DATE,
	emp_salario			INTEGER(7),
	emp_comision		INTEGER(7),
	dep_id				INTEGER(2)
	);
	
CREATE TABLE DEPARTAMENTOS (
	dep_id				INTEGER(2) PRIMARY KEY,
	dep_nombre			VARCHAR(15),
	dep_localización	VARCHAR (15)
	);
