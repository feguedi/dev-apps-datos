CREATE TABLE ORDENES (
	ORD_ID 				INTEGER(4) PRIMARY KEY,
	ORD_FECHA			DATE,
	ORD_PLANCOMISION	VARCHAR(1),
	ORD_FECHAENTREGA	DATE,
	ORD_TOTAL			DECIMAL(11,2),
    CLI_ID              INTEGER(6) NOT NULL
);


CREATE TABLE PRODUCTOSPORORDEN (
	PPO_ID 				INTEGER(4) PRIMARY KEY,
	PPO_PRECIOACTUAL 	DECIMAL(11,2),
	PPO_CANTIDAD 		INTEGER(8),
	PPO_TOTALLINEA 		DECIMAL(11,2),
    PRO_ID              INTEGER(8) NOT NULL
);
	
	
CREATE TABLE PRECIOS (
	PRO_ID              INTEGER(8) NOT NULL,
    PRE_FECHAINICIO     DATE,
	PRE_FECHAFIN		DATE,
	PRE_ESTANDAR		DECIMAL(11,2),
	PRE_MINIMO			DECIMAL(11,2)
);
	
	
CREATE TABLE CLIENTES (
	CLI_ID 				INTEGER(6) PRIMARY KEY,
	CLI_NOMBRE 			VARCHAR(45),
	CLI_DIRECCION 		VARCHAR(40),
	CLI_CIUDAD 			VARCHAR(30),
	CLI_DEPARTAMENTO 	VARCHAR(2),
	CLI_TELEFONO 		VARCHAR(11),
	CLI_CREDITOLIMITE 	DECIMAL(11,2),
	CLI_OBSERVACIONES 	CHAR(100),
    EMP_ID              INTEGER(4) NOT NULL    
);
	
	
CREATE TABLE PRODUCTOS (
	PRO_ID 				INTEGER(8) PRIMARY KEY,
	PRO_NOMBRE			VARCHAR(40)
);
	
	
CREATE TABLE BONIFICACIONES (
	EMP_NOMBRE			VARCHAR(10),
	EMP_CARGO			VARCHAR(10),
	EMP_SALARIO			INTEGER,
	EMP_COMISION		INTEGER
);
	
	
CREATE TABLE GRADOSDESALARIO (
	GRA_ID				NUMERIC,
	GRA_LIMITEINFERIOR	NUMERIC,
	GRA_LIMITESUPERIOR	NUMERIC
);
	
	
CREATE TABLE EMPLEADOS (
	EMP_ID				INTEGER(4) PRIMARY KEY, 
	EMP_NOMBRE			VARCHAR(10),
	EMP_CARGO			VARCHAR(10),
    EMP_JEFE            INTEGER(4) NOT NULL,
	EMP_FECHACONTRATO	DATE,
	EMP_SALARIO			DECIMAL(11,2),
	EMP_COMISION		DECIMAL(11,2),
    DEP_ID              INTEGER(2) NOT NULL
);
	
	
CREATE TABLE DEPARTAMENTOS (
	DEP_ID				INTEGER(2) PRIMARY KEY,
	DEP_NOMBRE			VARCHAR(15),
	DEP_LOCALIZACION	VARCHAR(15)
);


ALTER TABLE PRODUCTOSPORORDEN ADD
(
    FOREIGN KEY (PRO_ID) REFERENCES PRODUCTOS(PRO_ID)
);

    
ALTER TABLE CLIENTES ADD
(
    FOREIGN KEY (EMP_ID) REFERENCES EMPLEADOS(EMP_ID)
);


ALTER TABLE EMPLEADOS ADD 
(
    FOREIGN KEY (EMP_JEFE) REFERENCES EMPLEADOS(EMP_ID)
);


ALTER TABLE EMPLEADOS ADD
(
    FOREIGN KEY (DEP_ID) REFERENCES DEPARTAMENTOS(DEP_ID)
    
);


ALTER TABLE ORDENES ADD
(
    FOREIGN KEY (CLI_ID) REFERENCES CLIENTES(CLI_ID)
);


INSERT INTO DEPARTAMENTOS VALUES ('10', 'CONTABILIDAD',     'MEDELLIN');
INSERT INTO DEPARTAMENTOS VALUES ('20', 'INVESTIGACION',    'CALI');
INSERT INTO DEPARTAMENTOS VALUES ('30', 'VENTAS',           'BOGOTA');
INSERT INTO DEPARTAMENTOS VALUES ('40', 'OPERACIONES',      'BUCARAMANGA');

INSERT INTO EMPLEADOS VALUES ('1000', 'LOPEZ',      'PRESIDENTE',   NULL,   '07-08-01', '7000000',  NULL,       '10');
INSERT INTO EMPLEADOS VALUES ('1100', 'BENAVIDEZ',  'GERENTE',      '1000', '10-06-01', '2900000',  NULL,       '30');
INSERT INTO EMPLEADOS VALUES ('1200', 'CORDOBA',    'GERENTE',      '1000', '19-06-01', '2700000',  NULL,       '10');
INSERT INTO EMPLEADOS VALUES ('1300', 'JIMENEZ',    'GERENTE',      '1000', '12-07-01', '2775000',  NULL,       '20');
INSERT INTO EMPLEADOS VALUES ('2100', 'MARTINEZ',   'VENDEDOR',     '1100', '01-10-01', '1200000',  '1100000',  '30');
INSERT INTO EMPLEADOS VALUES ('2200', 'ARTEAGA',    'VENDEDOR',     '1100', '10-02-01', '1600000',  '700000',   '30');
INSERT INTO EMPLEADOS VALUES ('2300', 'TOLEDO',     'VENDEDOR',     '1100', '15-11-01', '1550000',  '0',        '30');
INSERT INTO EMPLEADOS VALUES ('2400', 'JARAMILLO',  'OFICINISTA',   '1100', '13-01-01', '900000',   NULL,       '30');
INSERT INTO EMPLEADOS VALUES ('2500', 'ZAMBRANO',   'VENDEDOR',     '1100', '22-02-01', '1250000',  '600000',   '30');
INSERT INTO EMPLEADOS VALUES ('3100', 'MARTINEZ',   'OFICINISTA',   '1200', '03-03-02', '1400000',  NULL,       '10');
INSERT INTO EMPLEADOS VALUES ('4100', 'FERNANDEZ',  'ANALISTA',     '1300', '05-12-01', '3050000',  NULL,       '20');
INSERT INTO EMPLEADOS VALUES ('4200', 'SANCHEZ',    'ANALISTA',     '1300', '04-10-02', '3005000',  NULL,       '20');
INSERT INTO EMPLEADOS VALUES ('5000', 'SANDOVAL',   'OFICINISTA',   '4100', '07-09-03', '855000',   NULL,       '20');
INSERT INTO EMPLEADOS VALUES ('6000', 'AGREDO',     'OFICINISTA',   '4200', '02-01-04', '1200000',  NULL,       '20');


INSERT INTO CLIENTES VALUES ('10', 'COMPUACCESORIOS',               'PASAJE COMERCIAL LOCALES 68 Y 71',         'POPAYAN',          'CA', '572-8318591');
INSERT INTO CLIENTES VALUES ('11', 'PUNTO NET COMPUTADORES',        'CARRERA 6 # 16N-19',                       'POPAYAN',          'CA', '572-8235352');
INSERT INTO CLIENTES VALUES ('12', 'PROWARE',                       'CARRERA 9 # 17N-79',                       'POPAYAN',          'CA', '572-8231320');
INSERT INTO CLIENTES VALUES ('13', 'SYSTEM PLUS',                   'UNICENTRO LOCAL 12 Y 13',                  'SANTIAGO DE CALI', 'VA', '572-8213375');
INSERT INTO CLIENTES VALUES ('14', 'COMPU TORRES',                  'CENTRO COMERCIAL CHIPI CHAPE LOCAL 15',    'SANTIAGO DE CALI', 'VA', '572-8318591');
INSERT INTO CLIENTES VALUES ('15', 'COMPU SUR',                     'CALLE 5 # 3-18',                           'PASTO',            'NA', '572-8255678');
INSERT INTO CLIENTES VALUES ('16', 'SOLUCIONES COMPUTACIONALES',    'CALLE 8 # 4-12',                           'NEIVA',            'HU', '573-6899071');
INSERT INTO CLIENTES VALUES ('17', 'PENSEMOS LTDA',                 'LA TRAIADA LOCAL 100-103',                 'BUCARAMANGA',      'SA', '577-6381602');
INSERT INTO CLIENTES VALUES ('18', 'COMPU TECH',                    'CARRERA 27 # 36-22',                       'BUCARAMANGA',      'SA', '577-6344010');


ALTER TABLE PRECIOS ADD
( 
    FOREIGN KEY (PRO_ID) REFERENCES PRODUCTOS(PRO_ID) 
);


INSERT INTO PRECIOS VALUES ('30100201','2002-10-07','03-10-06', '30000','24000');
INSERT INTO PRECIOS VALUES ('30100201','2003-10-07','04-03-05', '32000','25600');
INSERT INTO PRECIOS VALUES ('30100201','2004-03-06', NULL,      '35000','28000');
INSERT INTO PRECIOS VALUES ('30100202','2002-10-07','03-10-06', '39000','31200');
INSERT INTO PRECIOS VALUES ('30100202','2002-10-07','04-03-05', '42000','33600');
INSERT INTO PRECIOS VALUES ('30100202','2004-03-06', NULL,      '45000','36000');
INSERT INTO PRECIOS VALUES ('30100203','2002-10-07','03-09-06', '2400', '1900');
INSERT INTO PRECIOS VALUES ('30100203','2003-10-07', NULL,      '2800', '2400');
INSERT INTO PRECIOS VALUES ('30100204','2002-10-07','03-09-06', '4800', '3200');
INSERT INTO PRECIOS VALUES ('30100204','2003-10-07', NULL,      '5600', '4800');
INSERT INTO PRECIOS VALUES ('30100205','2002-03-07','02-03-06', '54000','40500');
INSERT INTO PRECIOS VALUES ('30100205','2002-10-07', NULL,      '58000','46400');
INSERT INTO PRECIOS VALUES ('30100206','2002-11-21', NULL,      '24000','18000');
INSERT INTO PRECIOS VALUES ('30100207','2002-11-21', NULL,      '12500','9400');
INSERT INTO PRECIOS VALUES ('30100208','2003-05-24', NULL,      '3400', '2800');
INSERT INTO PRECIOS VALUES ('30100209','2004-08-20', NULL,      '2400', '1750');
INSERT INTO PRECIOS VALUES ('30100210','2004-08-20', NULL,      '4000', '3200');


INSERT INTO PRODUCTOS VALUES ('30100201', 'SISTEMA OPERATIVO PROFESIONAL');
INSERT INTO PRODUCTOS VALUES ('30100202', 'SERVIDOR DE BASE DE DATOS PERSONAL');
INSERT INTO PRODUCTOS VALUES ('30100203', 'SERVIDOR DE BASE DE DATOS EMPRESARIAL');
INSERT INTO PRODUCTOS VALUES ('30100204', 'DISCO DURO 80 GB');
INSERT INTO PRODUCTOS VALUES ('30100205', 'PROCESADOR 3.2 GHZ');
INSERT INTO PRODUCTOS VALUES ('30100206', 'MOUSE INALAMBRICO');
INSERT INTO PRODUCTOS VALUES ('30100207', 'TARJETA DE SONIDO');
INSERT INTO PRODUCTOS VALUES ('30100208', 'IMPORTADO DE VIDE "IEE 1394"');
INSERT INTO PRODUCTOS VALUES ('30100209', 'MONITOR 17" PLANO LCD');
INSERT INTO PRODUCTOS VALUES ('30100210', 'TARJETA DE RED 10/100');


ALTER TABLE PRODUCTOSPORORDEN ADD
(
    FOREIGN KEY (ORD_ID) REFERENCES ORDENES(ORD_ID)
);


INSERT INTO PRODUCTOSPORORDEN VALUES ('452', '1', '30100203', '2800',   '20',     '5600');
INSERT INTO PRODUCTOSPORORDEN VALUES ('453', '2', '30100201', '56000',  '4',      '22400');
INSERT INTO PRODUCTOSPORORDEN VALUES ('454', '1', '30100205', '58000',  '3',      '17400');
INSERT INTO PRODUCTOSPORORDEN VALUES ('454', '2', '30100202', '42000',  '2',      '8400');
INSERT INTO PRODUCTOSPORORDEN VALUES ('455', '3', '30100201', '44000',  '10',     '44000');
INSERT INTO PRODUCTOSPORORDEN VALUES ('455', '1', '30100202', '45000',  '100',    '450000');
INSERT INTO PRODUCTOSPORORDEN VALUES ('455', '2', '30100203', '2800',   '500',    '140000');
INSERT INTO PRODUCTOSPORORDEN VALUES ('455', '3', '30100205', '58000',  '5',      '29000');
INSERT INTO PRODUCTOSPORORDEN VALUES ('455', '4', '30100206', '24000',  '50',     '120000');
INSERT INTO PRODUCTOSPORORDEN VALUES ('455', '5', '30100207', '9000',   '100',    '90000');
INSERT INTO PRODUCTOSPORORDEN VALUES ('455', '6', '30100208', '3400',   '10',     '3400');
INSERT INTO PRODUCTOSPORORDEN VALUES ('461', '1', '30100202', '45000',  '1',      '4500');
INSERT INTO PRODUCTOSPORORDEN VALUES ('462', '1', '30100201', '30000',  '100',    '300000');
INSERT INTO PRODUCTOSPORORDEN VALUES ('462', '2', '30100202', '40500',  '20',     '81000');
INSERT INTO PRODUCTOSPORORDEN VALUES ('462', '3', '30100207', '10000',  '150',    '150000');


INSERT INTO ORDENES VALUES ('452', '05-02-28', '2',     '12', '05-03-15', '56000');
INSERT INTO ORDENES VALUES ('453', '05-02-28', NULL,    '12', '05-02-28', '224000');
INSERT INTO ORDENES VALUES ('454', '05-03-10', '1',     '16', '05-03-25', '698000');
INSERT INTO ORDENES VALUES ('455', '05-04-08', '1',     '16', '05-04-24', '8324000');
INSERT INTO ORDENES VALUES ('461', '05-10-06', '2',     '12', '05-10-06', '45000');
INSERT INTO ORDENES VALUES ('462', '05-10-10', '3',     '14', '05-10-15', '5860000');
INSERT INTO ORDENES VALUES ('463', '05-10-27', NULL,    '18', '05-10-27', '6400000');
INSERT INTO ORDENES VALUES ('464', '05-10-27', NULL,    '12', '05-10-31', '23940000');
INSERT INTO ORDENES VALUES ('465', '05-10-27', NULL,    '17', '05-11-01', '710000');
INSERT INTO ORDENES VALUES ('466', '05-10-29', NULL,    '13', '05-11-05', '764000');
INSERT INTO ORDENES VALUES ('467', '05-10-31', NULL,    '15', '05-11-26', '46370000');
INSERT INTO ORDENES VALUES ('469', '05-10-17', NULL,    '14', '05-10-30', '1260000');
INSERT INTO ORDENES VALUES ('470', '05-12-05', NULL,    '10', '05-12-05', '4450000');
INSERT INTO ORDENES VALUES ('471', '05-12-08', NULL,    '10', '05-09-26', '73000');


INSERT INTO GRADOSDESALARIO VALUES ('1', '0',       '1100000');
INSERT INTO GRADOSDESALARIO VALUES ('2', '1100001', '1500000');
INSERT INTO GRADOSDESALARIO VALUES ('3', '1500001', '2000000');
INSERT INTO GRADOSDESALARIO VALUES ('4', '2000001', '3000000');
INSERT INTO GRADOSDESALARIO VALUES ('5', '3000001', '9999999');


INSERT INTO BONIFICACIONES VALUES (NULL, NULL, NULL, NULL);


