-Ejercicio 1: Selección
SELECT *
FROM EMPLEADOS
WHERE dep_id = 30

-Ejercicio 2: Proyección
SELECT dep_id, dep_nombre, dep_localización, 'Activo'
FROM DEPARTAMENTOS

-Ejercicio 3: Selección
SELECT emp_id
FROM EMPLEADOS
WHERE emp_nombre = '&u&'

-Ejercicio 4: Selección
SELECT emp_salario
FROM EMPLEADOS
WHERE emp_salario >= 855000 AND emp_salario < 1000000

-Ejercicio 5: Selección
SELECT emp_nombre
FROM EMPLEADOS
WHERE emp_cargo = 'Oficinista' OR emp_cargo = 'Vendedor' OR emp_cargo = 'Analista'

-Ejercicio 6: Selección
SELECT j.emp_nombre
FROM EMPLEADOS e, EMPLEADOS j
WHERE e.emp_nombre = j.emp_id AND e.emp_nombre = e.emp_jefe

-Ejercicio 7: Unión
SELECT emp_fechacontrato, dep_nombre
FROM DEPARTAMENTOS D, EMPLEADOS E
WHERE emp_fechacontrato > '03-01-00' AND dep_nombre = '&s'

-Ejercicio 8: Selección
SELECT emp_nombre
FROM EMPLEADOS
WHERE emp_comision = NULL

-Ejercicio 9: Selección
SELECT *
FROM EMPLEADOS
WHERE emp_nombre = 'J&' AND emp_fechacontrato > '01-01-01' AND (emp_salario > 1000000 AND emp_salario < 3000000) AND dep_nombre = '_____S'

-Ejercicio 10: Unión
SELECT e.emp_nombre, j.emp_jefe
FROM EMPLEADOS e, EMPLEADOS j
WHERE e.emp_nombre = j.emp_jefe
